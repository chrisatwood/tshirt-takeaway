<?php

session_start();
$ret = array();
if ( isset($_GET['fileId']) ) {

    parse_str($_SERVER['QUERY_STRING']);

    if($fileId !== '')
    {
        $filePathInfo = pathinfo($fileId);
        $details = array();
        $details['name'] = $filePathInfo['basename'];
        $details['path'] = $fileId;
        $details['size'] = filesize($fileId);
        $ret[] = $details;
    }
}
echo json_encode($ret);

?>