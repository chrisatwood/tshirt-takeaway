<?php

    require_once('common/common.php');


	$output_dir = "uploads/";
	session_start();
	$session_id = session_id();
	$directoryPath = $output_dir.$session_id;

	if(isset($_POST["op"]) && $_POST["op"] == "delete" && isset($_POST['name']))
	{
		$fileName = $_POST['name'];
		$fileName=str_replace("..",".",$fileName); 	
		$filePath = $directoryPath."/". $fileName;
		if (file_exists($filePath)) 
		{
			unlink($filePath);
		}
		else
		{
			if(file_exists($fileName))
			{
				unlink($fileName);
			}
		}
	}


 ?>