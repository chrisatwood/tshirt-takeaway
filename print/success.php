<?php


  function storeOrderHeader($conn, $jsonObj)
    {
        // prepare sql and bind parameters
        $stmt = $conn->prepare("INSERT INTO OrderHeader (Name, EmailAddress, PhoneNumber, OrderAddressLine1, OrderAddressLine2, OrderAddressLine3, OrderPostCode, OrderCity, DeliveryAddressLine1, DeliveryAddressLine2, DeliveryAddressLine3, DeliveryCity, DeliveryPostCode)" 
        ."VALUES (:Name, :EmailAddress, :PhoneNumber, :OrderAddressLine1, :OrderAddressLine2, :OrderAddressLine3, :OrderPostCode, :OrderCity, :DeliveryAddressLine1, :DeliveryAddressLine2, :DeliveryAddressLine3, :DeliveryCity, :DeliveryPostCode)");
    
        $stmt->bindParam(':Name', $jsonObj['detailStage6_'.$jsonObj['totalNumberOfDetails']]['firstName']);
        $stmt->bindParam(':EmailAddress', $jsonObj['detailStage6_'.$jsonObj['totalNumberOfDetails']]['emailAddress']);
        $stmt->bindParam(':PhoneNumber', $jsonObj['detailStage6_'.$jsonObj['totalNumberOfDetails']]['phoneNumber']);

        $stmt->bindParam(':OrderAddressLine1', $jsonObj['detailStage7_'.$jsonObj['totalNumberOfDetails']]['addressLine1Billing']);
        $stmt->bindParam(':OrderAddressLine2', $jsonObj['detailStage7_'.$jsonObj['totalNumberOfDetails']]['addressLine2Billing']);
        $stmt->bindParam(':OrderAddressLine3', $jsonObj['detailStage7_'.$jsonObj['totalNumberOfDetails']]['addressLine3Billing']);
        $stmt->bindParam(':OrderPostCode', $jsonObj['detailStage7_'.$jsonObj['totalNumberOfDetails']]['postCodeBilling']);
        $stmt->bindParam(':OrderCity', $jsonObj['detailStage7_'.$jsonObj['totalNumberOfDetails']]['cityBilling']);

        $stmt->bindParam(':DeliveryAddressLine1', $jsonObj['detailStage7_'.$jsonObj['totalNumberOfDetails']]['addressLine1Delivery']);
        $stmt->bindParam(':DeliveryAddressLine2', $jsonObj['detailStage7_'.$jsonObj['totalNumberOfDetails']]['addressLine2Delivery']);
        $stmt->bindParam(':DeliveryAddressLine3', $jsonObj['detailStage7_'.$jsonObj['totalNumberOfDetails']]['addressLine3Delivery']);
        $stmt->bindParam(':DeliveryCity', $jsonObj['detailStage7_'.$jsonObj['totalNumberOfDetails']]['cityDelivery']);
        $stmt->bindParam(':DeliveryPostCode', $jsonObj['detailStage7_'.$jsonObj['totalNumberOfDetails']]['postCodeDelivery']);

        $stmt->execute();
        
        $id = $conn -> lastInsertId();

        return $id;
    }

    function storeOrderDetail($conn, $headerId, $jsonObj)
    {
        $totalNumberOfDetails = intval($jsonObj["totalNumberOfDetails"]);
        $runningTotal = 0;

        $stmt = $conn->prepare("INSERT INTO OrderDetail(Colour, GarmentType, Quality, Quantity, SelectedColourID, Size2XLarge, SizeXLarge, SizeLarge, SizeMedium, SizeSmall, ColourFront1, ColourFront2, ColourFront3, ColourFront4, ColourBack1, ColourBack2, ColourBack3, ColourBack4, ColourSleeve1, ColourSleeve2, ColourSleeve3, ColourSleeve4, TotalPrice, OrderHeaderID, ColourFrontTotal, ColourBackTotal, ColourSleeveTotal, AnyOtherInformation )" 
        ."VALUES (:Colour, :GarmentType, :Quality, :Quantity, :SelectedColourID, :Size2XLarge, :SizeLarge, :SizeXLarge, :SizeMedium, :SizeSmall, :ColourFront1, :ColourFront2, :ColourFront3, :ColourFront4, :ColourBack1, :ColourBack2, :ColourBack3, :ColourBack4, :ColourSleeve1, :ColourSleeve2, :ColourSleeve3, :ColourSleeve4, :TotalPrice, :OrderHeaderID, :ColourFrontTotal, :ColourBackTotal, :ColourSleeveTotal, :AnyOtherInformation)");


        for ($x = 1; $x <= $totalNumberOfDetails; $x++) {

            //get out the relevant details and insert them into the database

            $stmt->bindParam(':Colour', $jsonObj['detailStage1_'.$x]['colour']);
            $stmt->bindParam(':GarmentType', $jsonObj['detailStage1_'.$x]['garmentType']);
            $stmt->bindParam(':Quality', $jsonObj['detailStage1_'.$x]['quality']);
            $stmt->bindParam(':Quantity', $jsonObj['detailStage1_'.$x]['quantity']);
            $stmt->bindParam(':SelectedColourID', $jsonObj['detailStage1_'.$x]['selectedColourId']);
            $stmt->bindParam(':Size2XLarge', $jsonObj['detailStage1_'.$x]['size2XLarge']);
            $stmt->bindParam(':SizeLarge', $jsonObj['detailStage1_'.$x]['sizeLarge']);
            $stmt->bindParam(':SizeMedium', $jsonObj['detailStage1_'.$x]['sizeMedium']);
            $stmt->bindParam(':SizeSmall', $jsonObj['detailStage1_'.$x]['sizeSmall']);
            $stmt->bindParam(':SizeXLarge', $jsonObj['detailStage1_'.$x]['sizeXLarge']);

            $stmt->bindParam(':ColourFrontTotal', $jsonObj['detailStage4_'.$x]['colourFrontTotal']);
            $stmt->bindParam(':ColourFront1', $jsonObj['detailStage4_'.$x]['colourFront1']);
            $stmt->bindParam(':ColourFront2', $jsonObj['detailStage4_'.$x]['colourFront2']);
            $stmt->bindParam(':ColourFront3', $jsonObj['detailStage4_'.$x]['colourFront3']);
            $stmt->bindParam(':ColourFront4', $jsonObj['detailStage4_'.$x]['colourFront4']);
            
            $stmt->bindParam(':ColourBackTotal', $jsonObj['detailStage4_'.$x]['colourBackTotal']);
            $stmt->bindParam(':ColourBack1', $jsonObj['detailStage4_'.$x]['colourBack1']);
            $stmt->bindParam(':ColourBack2', $jsonObj['detailStage4_'.$x]['colourBack2']);
            $stmt->bindParam(':ColourBack3', $jsonObj['detailStage4_'.$x]['colourBack3']);
            $stmt->bindParam(':ColourBack4', $jsonObj['detailStage4_'.$x]['colourBack4']);

            $stmt->bindParam(':ColourSleeveTotal', $jsonObj['detailStage4_'.$x]['colourSleeveTotal']);
            $stmt->bindParam(':ColourSleeve1', $jsonObj['detailStage4_'.$x]['colourSleeve1']);
            $stmt->bindParam(':ColourSleeve2', $jsonObj['detailStage4_'.$x]['colourSleeve2']);
            $stmt->bindParam(':ColourSleeve3', $jsonObj['detailStage4_'.$x]['colourSleeve3']);
            $stmt->bindParam(':ColourSleeve4', $jsonObj['detailStage4_'.$x]['colourSleeve4']);

            $stmt->bindParam(':TotalPrice', $jsonObj['detailStage4_'.$x]['totalPrice']);

            $stmt->bindParam(':AnyOtherInformation', $jsonObj['detailStage5_'.$x]['anyOtherNotes']);

            $stmt->bindParam(':OrderHeaderID', $headerId);

            $stmt->execute();

            $runningTotal += intval($jsonObj['detailStage4_'.$x]['totalPrice']);
        } 

        return $runningTotal;
    }

    function updateOrderTotal($conn, $headerId, $orderTotal)
    {
        // prepare sql and bind parameters
        $stmt = $conn->prepare("UPDATE OrderHeader SET OrderTotal = :orderTotal WHERE ID = :orderID");
    
        $stmt->bindParam(':orderTotal', $orderTotal);
        $stmt->bindParam(':orderID', $headerId);

        $stmt->execute();
    }

    function moveFilesToOrderFolder($orderID)
    {
        $output_dir = "uploads/";

        $session_id = session_id();
        $oldPath = $output_dir.$session_id."/";

        $newPath = $output_dir.$orderID."/";

        if(file_exists($oldPath))
        {
            rename($oldPath, $newPath);
        }
    }

    function deleteOrderFromDatabase($conn, $orderID)
    {
      try
      {

        $stmt = $conn->prepare("DELETE FROM OrderDetail WHERE OrderHeaderID = :orderID");
        $stmt->bindParam(':orderID', $orderID);
        $stmt->execute();

        $stmt2 = $conn->prepare("DELETE FROM OrderHeader WHERE ID = :orderID");
        $stmt2->bindParam(':orderID', $orderID);
        $stmt2->execute();
      }
      catch(Exception $e)
      {

      }
    }



  session_start();

  require_once('vendor/autoload.php');
  include_once('common/common.php');
  include_once('common/email.php');


if (isset($_POST["tokenId"]))
{
  $orderID = 0;
  $conn = new PDO($DB_CONNECTION_STRING, $DB_USER_NAME, $DB_PASSWORD);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $errorCode = 0;
  
  try
  {
      //STAGE 1 - store all in database
      $jsonObj = json_decode($_POST['postData'], true); //convert into arrays rather than object

      $session_id = session_id();//needed to move files around

      //store data into the datbase
      $insertId = storeOrderHeader($conn, $jsonObj);
      $runningTotal = storeOrderDetail($conn, $insertId, $jsonObj);
      updateOrderTotal($conn, $insertId, $runningTotal);
        
      $_SESSION["amount"] = $runningTotal;
      $emailAddress = $jsonObj['detailStage6_'.$jsonObj['totalNumberOfDetails']]['emailAddress'];
      $_SESSION["orderID"] = $insertId;
      $orderID = $insertId;
      //stage 2 -- take payment
    
      $token = $_POST['tokenId'];
      
      $amount = floatval($_SESSION['amount']) * 100;

      $stripe = array(
      "secret_key"      => $STRIPE_SECRET_KEY,
      "publishable_key" => $STRIPE_PUBLISHABLE_KEY
      );

      \Stripe\Stripe::setApiKey($stripe['secret_key']);
      
      $customer = \Stripe\Customer::create(array(
          'email' => $emailAddress,
          'source'  => $token
      ));

      $charge = \Stripe\Charge::create(array(
          'customer' => $customer->id,
          'amount'   => $amount,
          'currency' => 'GBP'
      ));

      //stage 3 -- move files around to the correct place

      //successfully taken payment, and put data into the database, so issue emails
      $orderID = $_SESSION["orderID"];
      
      moveFilesToOrderFolder($insertId);

      //stage 4 - send out the emails

      sendCustomerEmail($orderID, $emailAddress, $SEND_EMAIL, $EMAIL_FROM);
      sendBackOfficeEmail($orderID, $ADMIN_EMAIL_ADDRESS, $SEND_EMAIL, $EMAIL_FROM);
  }
  catch(\Stripe\Error\Card $e) {
    // Since it's a decline, \Stripe\Error\Card will be caught
    $body = $e->getJsonBody();
    $err  = $body['error'];

    // print('Status is:' . $e->getHttpStatus() . "\n");
    // print('Type is:' . $err['type'] . "\n");
    // print('Code is:' . $err['code'] . "\n");
    // // param is '' in this case
    // print('Param is:' . $err['param'] . "\n");
    // print('Message is:' . $err['message'] . "\n");
    
    //any errors, delete from the database, and show error message
    deleteOrderFromDatabase($conn, $orderID);
    $errorCode = 1;
  } catch (\Stripe\Error\RateLimit $e) {
    // Too many requests made to the API too quickly

    deleteOrderFromDatabase($conn, $orderID);
    $errorCode = 2;
  } catch (\Stripe\Error\InvalidRequest $e) {
    // Invalid parameters were supplied to Stripe's API

    deleteOrderFromDatabase($conn, $orderID);
    $errorCode = 3;
  } catch (\Stripe\Error\Authentication $e) {
    // Authentication with Stripe's API failed
    // (maybe you changed API keys recently)

    deleteOrderFromDatabase($conn, $orderID);
    $errorCode = 4;
  } catch (\Stripe\Error\ApiConnection $e) {
    // Network communication with Stripe failed

    deleteOrderFromDatabase($conn, $orderID);
    $errorCode = 5;
  } catch (\Stripe\Error\Base $e) {
    // Display a very generic error to the user, and maybe send
    // yourself an email
    $errorCode = 6;
  } catch (Exception $e) {
    // Something else happened, completely unrelated to Stripe

    deleteOrderFromDatabase($conn, $orderID);
    $errorCode = 7;
  }
}

?>


<!DOCTYPE html>
  <html>
    <head>
      <title>Tshirt Takeaway</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
       <link href="css/app.css" rel="stylesheet">
</head>
<?php
 if($errorCode === 0)
 {
   
?>
  <script>localStorage.clear();</script>
<?php 
 }
 ?>
<div class="wrapper">
  <header class="header">
    <div class="container">      
      <h1 class="logo">Tshirt-Takeaway</h1>
    </div>
  </header>
  <div class="content">
    <div class="container container--center " data-err="<?php echo $errorCode;?>">
      <?php if($errorCode === 0)
      {
        ?>
          <h2 class="title">Thank You</h2>
          <p class="longform">We have received your order. Please check your inbox for a confirmation email.</p>
          <p class="longform last">We'll be in touch soon with a digital proof of your order. In the meantime, if you have any queries please email us <a href="mailto:info@tshirttakeaway.co.uk">info@tshirttakeaway.co.uk</a>, quoting order reference: <span id="tokenId"><?php echo $_SESSION["orderID"];?> </span>.</p>
      <?php 
      }
      else if($errorCode === 1)
      {

        ?>
            <h2 class="title">Oops!</h2>
            <p class="longform">Your Payment Was Not Successful</p>
            <p class="longform last">Please check your payment details and <a href="http://tshirttakeaway.co.uk/print">submit your order again</a>. If you are still having issues, please contact us on: 029 2056 6577.</p>
        <?php 
              }
              else if($errorCode === 2)
              {

        ?>
            <h2 class="title">Oops!</h2>
            <p class="longform">Your Payment Was Not Successful</p>
            <p class="longform last">Please check your payment details and <a href="http://tshirttakeaway.co.uk/print">submit your order again</a>. If you are still having issues, please contact us on: 029 2056 6577.</p>
        <?php 
              }
              else if($errorCode === 3)
              {

        ?>
            <h2 class="title">Oops!</h2>
            <p class="longform">Your Payment Was Not Successful</p>
            <p class="longform last">Please check your payment details and <a href="http://tshirttakeaway.co.uk/print">submit your order again</a>. If you are still having issues, please contact us on: 029 2056 6577.</p>
        <?php 
              }
              else if($errorCode == 4)
              {

        ?>
            <h2 class="title">Oops!</h2>
            <p class="longform">Your Payment Was Not Successful</p>
            <p class="longform last">Please check your payment details and <a href="http://tshirttakeaway.co.uk/print">submit your order again</a>. If you are still having issues, please contact us on: 029 2056 6577.</p>
        <?php 
              }
              else if($errorCode === 5)
              {

        ?>
            <h2 class="title">Oops!</h2>
            <p class="longform">Your Payment Was Not Successful</p>
            <p class="longform last">Please check your payment details and <a href="http://tshirttakeaway.co.uk/print">submit your order again</a>. If you are still having issues, please contact us on: 029 2056 6577.</p>
        <?php 
              }
              else if($errorCode === 6)
              {

        ?>
            <h2 class="title">Oops!</h2>
            <p class="longform">Your Payment Was Not Successful</p>
            <p class="longform last">Please check your payment details and <a href="http://tshirttakeaway.co.uk/print">submit your order again</a>. If you are still having issues, please contact us on: 029 2056 6577.</p>

          <?php   }
              else if($errorCode === 7)
              {

        ?>
            <h2 class="title">Oops!</h2>
            <p class="longform">Your Payment Was Not Successful</p>
            <p class="longform last">Please check your payment details and <a href="http://tshirttakeaway.co.uk/print">submit your order again</a>. If you are still having issues, please contact us on: 029 2056 6577.</p>
        <?php 
              }
        ?>
    </div>
  </div>
</div>
<footer class="footer">
  <div class="container">
    <img src="img/logo-long.svg" alt="Tshirt Takeaway" class="footer__logo">
      <section class="social">
        <ul>
          <li>
            <a class="facebook social-icon" href="#" target="_blank">
              <span>Facebook</span>
            </a>
          </li>
          <li>
            <a class="twitter social-icon" href="#" target="_blank">
              <span>Twitter</span>
            </a>
          </li>
          <li>
            <a class="instagram social-icon" href="#" target="_blank">
              <span>Instagram</span>
            </a>
          </li>
        </ul>
      </section>
    </div>
  </footer>
</body>
</html>
