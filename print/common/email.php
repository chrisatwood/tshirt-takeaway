<?php


function sendCustomerEmail($orderID, $emailAddress, $sendEmail, $emailFrom)
{

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= 'From: '.$emailFrom. "\r\n";

    $template = file_get_contents("common/emailTemplates/order-confirmation.html");

    $template = str_replace('{{orderID}}', $orderID, $template);
    
    if($sendEmail == true)
    {
        mail($emailAddress, "We've got your order!", $template, $headers);
    }
}

function sendBackOfficeEmail($orderID, $emailAddress, $sendEmail, $emailFrom)
{

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= 'From: '.$emailFrom. "\r\n";


    $template = file_get_contents("common/emailTemplates/va-order-confirmation.html");
    
    $template = str_replace('{{orderID}}', $orderID, $template);

    if($sendEmail == true)
    {
        mail($emailAddress, "New Order Recieved!", $template, $headers);
    }
}

?>