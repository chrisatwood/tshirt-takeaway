<?php

include_once('common/common.php');
session_start();
//set the paging limit here
$limit = $MAX_ITEMS_PER_SUMMARY_PAGE;

$loggedIn = isset($_SESSION["LOGGED_IN"]);

if($loggedIn == true)
{


    $pdo = new PDO($DB_CONNECTION_STRING, $DB_USER_NAME, $DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $total = $pdo->query('SELECT  COUNT(*) FROM OrderHeader')->fetchColumn();

    $pages = ceil($total / $limit);
    // What page are we currently on?
    $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array('options' => array(
            'default'   => 1,
            'min_range' => 1,
        ),
    )));
    $offset = ($page - 1)  * $limit;
    $start = $offset + 1;
    $end = min(($offset + $limit), $total);

    // The "back" link
    $prevlink = ($page > 1) ? '<a class="btn--smaller prev" href="?page=' . ($page - 1) . '" title="Previous page">Back</a>' : '<a href="?page=' . ($page - 1) . '" title="Previous page"></a>';

    // The "forward" link
    $nextlink = ($page < $pages) ? '<a class="btn--smaller next" href="?page=' . ($page + 1) . '" title="Next page">Next</a>' : '';



    $query = "SELECT * FROM OrderHeader ORDER BY ID DESC LIMIT :limit OFFSET :offset"; //latest on top
    $stmt = $pdo->prepare($query); 
    $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
    $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
    $stmt->execute();
    $data = $stmt->fetchAll();


}
else
{
  //You need to redirect
    header("Location: AdminLogin.php"); /* Redirect browser */
    exit();
}
?>

<!DOCTYPE html>
  <html>
    <head>
      <title>Tshirt Takeaway</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
       <link href="css/app.css" rel="stylesheet">
    <script type="text/javascript" src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="icons/favicon-16x16.png">
<link rel="manifest" href="icons/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="icons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
</head>
<div class="wrapper">
  <header class="header">
    <div class="container">      
      <h1 class="logo">Tshirt-Takeaway</h1>
    </div>
  </header>
  <div class="content">
    <div class="container container--center">
      <h2 class="title">Orders</h2>
      <p class="longform last">New orders submitted via the website will be shown here. Please click for more information and to download artwork.</p>
    </div>

    <div class="container">
          <ul class="adminorder__summary">
          <?php 
          foreach($data as $row) 
          {
            ?>

           <li class="adminsummary__item">
                <ul class="adminsummary__row">
                    <li class="adminsummary-item__date">Ordered: <span><?php $time = strtotime($row['OrderDate']); $newformat = date('d/m/Y',$time); echo $newformat; ?></span></li>
                    <li class="adminsummary-item__ref">Ref: <span><?php echo $row['ID'] ?></span></li>
                    <li class="adminsummary-item__total">Paid: <span>£<?php echo $row['OrderTotal'] ?></span></li>
                </ul>
                <div class="adminsummary__edit"><a class="btn--hollow" href="AdminOrderSummary.php?orderNumber=<?php echo $row['ID'] ?>">View Order Details</a></div> 
            </li>
             <?php
                }
            ?>
          </ul>
    </div>
  </div>
  <div class="content">
    <div class="container">
      <section class="pagination">
          <?php
              echo '', $prevlink, $nextlink, ' ';
              ?>
      </section>
    </div>
  </div>
</div>
<footer class="footer">
  <div class="container">
    <img src="img/logo-long.svg" alt="Tshirt Takeaway" class="footer__logo">
      <section class="social">
        <ul>
          <li>
            <a class="facebook social-icon" href="https://www.facebook.com/tshirttakeawayuk" target="_blank">
              <span>Facebook</span>
            </a>
          </li>
          <li>
          <a href="https://twitter.com/tshirt_takeaway" class="twitter social-icon" href="#" target="_blank">
              <span>Twitter</span>
            </a>
          </li>
          <li>
            <a href="#" class="instagram social-icon" href="https://www.instagram.com/tshirt_takeaway/" target="_blank">
              <span>Instagram</span>
            </a>
          </li>
        </ul>
      </section>
    </div>
  </footer>
</body>
</html>