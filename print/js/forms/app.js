/**
 * system constants
 */
var DEFAULT_SHOW_HIDE_SPEED = 300;
var NUMBER_OF_WORKING_DAYS_FOR_ORDER = 5;
var PAYMENT_SUCCESS = false;
var SUBMIT_OBJECT = {};
//change this to point to the correct URL, or use this for testing purposes
var CUSTOMER_DATA_URL = "customerData.json";
var HAVE_BOUND_ADDRESS = false;
var DETAIL_SECTIONS = [
  "#detailStage1",
  "#detailStage2",
  "#detailStage3",
  "#detailStage4",
  "#detailStage5",
  "#detailStage6",
  "#detailStage7",
  "#detailStage8",
];

bindFrontFileUploadHandlers();

var handler = StripeCheckout.configure({
  //key: "pk_test_6pRNASCoBOKtIshFeQd4XMUh",
  key: "pk_live_SOuYY5p8bXbTiIJbcnf3oil1",
  image: "https://tshirttakeaway.co.uk/print/img/stripe.png",
  locale: "auto",
  token: function(token) {
    //sessionStorage["tokenId"] = token.id;
    if (token.id) {
      $("#tokenId").attr("value", token.id);
      $("#postData").attr("value", JSON.stringify(SUBMIT_OBJECT));
      $("#detailStage8_form").submit();
    }
  }
});

function handleStripePaymentIntegration(summaryObj) {
  var amountFormatted = summaryObj.amount * 100; //need to convert this into pounds, as it will default to pence as standard.
  handler.open({
    name: summaryObj.orderTitle,
    description: summaryObj.orderDescription,
    zipCode: true,
    amount: amountFormatted,
    billingAddress: false,
    currency: "GBP",
    shippingAddress: false,
    email: summaryObj.email,
    allowRememberMe: false,
    bitCoin: false
  });
}

var UPLOAD_URL = "upload.php";
var SHOW_DELETE = true;
var SHOW_PREVIEW = false;
var PREVIEW_HEIGHT = "100px";
var PREVIEW_WIDTH = "100px";
var MAX_FILE_SIZE = 20000 * 1024;
var STATUS_BAR_WIDTH = 600;
var DRAG_DROP_WIDTH = 600;

var handleFileUploadFront1 = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "FrontA",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["FrontA_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["FrontA_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadFrontPart1").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["FrontA_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

var handleFileUploadFront2 = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "FrontB",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["FrontB_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["FrontB_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadFrontPart2").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["FrontB_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

var handleFileUploadFront3 = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "FrontC",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["FrontC_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["FrontC_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadFrontPart3").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["FrontC_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

var handleFileUploadFront4 = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "FrontD",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["FrontD_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["FrontD_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadFrontPart4").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["FrontD_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

var handleFileUploadFront5 = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "FrontE",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["FrontE_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["FrontE_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadFrontPart5").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["FrontE_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

var handleFileUploadFront6 = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "FrontF",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["FrontF_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["FrontF_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadFrontPart6").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["FrontF_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

var handleFileUploadFront7 = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "FrontG",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["FrontG_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["FrontG_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadFrontPart7").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["FrontG_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

var handleFileUploadFront8 = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "FrontH",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["FrontH_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["FrontH_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadFrontPart8").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["FrontH_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

var handleFileUploadFront9 = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "FrontI",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["FrontI_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["FrontI_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadFrontPart9").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["FrontI_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

/**
 * 
 * START BACK UPLOAD OPTIONS
 * 
 */

var handleFileUploadBackA = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "BackA",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["BackA_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["BackA_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadBackPartA").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["BackA_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

var handleFileUploadBackB = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "BackB",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["BackB_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["BackB_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadBackPartB").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["BackB_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

var handleFileUploadBackC = {
  url: UPLOAD_URL,
  dragDrop: true,
  fileName: "fileUpload",
  returnType: "json",
  showDelete: SHOW_DELETE,
  showDownload: false,
  statusBarWidth: STATUS_BAR_WIDTH,
  dragdropWidth: DRAG_DROP_WIDTH,
  maxFileSize: MAX_FILE_SIZE,
  maxFileCount: 1,
  showPreview: SHOW_PREVIEW,
  previewHeight: PREVIEW_HEIGHT,
  previewWidth: PREVIEW_WIDTH,
  multiple: false,
  dynamicFormData: function() {
    var data = {
      stage: "BackC",
      detail: getCurrentDetailNumber()
    };
    return data;
  },
  onSuccess: function(files, data, xhr, pd) {
    localStorage["BackC_" + getCurrentDetailNumber()] = data;
  },
  onLoad: function(obj) {
    $.ajax({
      cache: false,
      url: "load.php",
      dataType: "json",
      data: { fileId: localStorage["BackC_" + getCurrentDetailNumber()] },
      success: function(data) {
        var outerLi = $("#fileUploadBackPartC").parent();
        outerLi.children(".ajax-file-upload-container").empty();
        for (var i = 0; i < data.length; i++) {
          obj.createProgress(data[i]["name"], data[i]["path"], data[i]["size"]);
        }
      }
    });
  },
  deleteCallback: function(data, pd) {
    for (var i = 0; i < data.length; i++) {
      $.post("delete.php", { op: "delete", name: data[i] }, function(
        resp,
        textStatus,
        jqXHR
      ) {
        localStorage["BackC_" + getCurrentDetailNumber()] = "";
      });
    }
  }
};

/*
*   UTILITY FUNCTIONS
*/
/**
 * 
 * @param {ID of the form to store into local storage} formId 
 */
function storeFormData(formId) {
  if (formId === "") return;

  var theFormId = getFormKey(formId);
  var jsonData = $(theFormId).serializeJSON();

    formId = formId + "_" + getCurrentDetailNumber();
    localStorage.setItem(formId, jsonData);
}

function getFormData(formId) {
  var data = {};
  //var isDetail = DETAIL_SECTIONS.includes(formId);
  var isDetail = false;
  if (
    formId === "#detailStage1" ||
    formId === "#detailStage2" ||
    formId === "#detailStage3" ||
    formId === "#detailStage4" ||
    formId === "#detailStage5" ||
    formId === "#detailStage6" ||
    formId === "#detailStage7" ||
    formId === "#detailStage8"
  ) {
    isDetail = true;
  }

  if (isDetail) {
    formId2 = formId + "_" + getCurrentDetailNumber();
    data = localStorage.getItem(formId2);
  } else {
    data = localStorage.getItem(formId);
  }

  return JSON.parse(data);
}

function getFormKey(formId) {
  var retVal = "form" + formId + "_form";
  return retVal;
}

function dataBindSection(formId) {
  var formKey = getFormKey(formId);
  var formData = getFormData(formId);
  if (formData != null) {
    //should probably clear the form before we databind it for all requests!
    populateForm($(formKey), formData);
  } else {
    //it could a detail section pre-populated, so we want it to be empty at this point...
    //clearForm(formKey);
  }
}

function GetDeliveryDate() {
  var tdate = new Date();
  var theNewDate = addWorkDays(tdate, NUMBER_OF_WORKING_DAYS_FOR_ORDER);
  return dateFormat(theNewDate, "dddd dd mmmm yyyy");
}

function getAddressDetails() {
  var key = "#detailStage7";
  return getFormData(key);
}

function storeAddressDetails(jsonObj) {
  var key = "#detailStage7_1";
  var objToStore = JSON.stringify(jsonObj);
  localStorage.setItem(key, objToStore);
}

function getOrderSummary() {
  var dataObj = localStorage.getItem("orderSummary");
  var retObj = JSON.parse(dataObj);
  return retObj;
}

function storeCurrentGarnmentColour(colour) {
  var garementColourKey = "garmentColour" + getCurrentDetailNumber();
  localStorage[garementColourKey] = colour;
}

function getCurrentGarmentColour() {
  var garementColourKey = "garmentColour" + getCurrentDetailNumber();
  var data = localStorage.getItem(garementColourKey);
  if (data === null) return "";
  else return data;
}

function loadColourDropDownValues() {
  var selectedColour = getCurrentGarmentColour();
  if (selectedColour === "Natural" || selectedColour === "White") {
    var frontExists = false;
    $("#colourFront option").each(function() {
      if (this.value == "fullColour") {
        frontExists = true;
        return false;
      }
    });
    if (frontExists === false) {
      $("#colourFront").append(
        $("<option>", {
          value: "fullColour",
          text: "Full Colour"
        })
      );
    }
    var backExists = false;
    $("#colourBack option").each(function() {
      if (this.value == "fullColour") {
        backExists = true;
        return false;
      }
    });
    if (backExists === false) {
      $("#colourBack").append(
        $("<option>", {
          value: "fullColour",
          text: "Full Colour"
        })
      );
    }
    var sleeveExists = false;
    $("#colourSleeveSelectDropDown option").each(function() {
      if (this.value == "fullColour") {
        sleeveExists = true;
        return false;
      }
    });
    if (sleeveExists === false) {
      $("#colourSleeveSelectDropDown").append(
        $("<option>", {
          value: "fullColour",
          text: "Full Colour"
        })
      );
    }
  } else {
    //remove the full colour option
    $("#colourFront").find("option[value=fullColour]").remove();
    $("#colourBack").find("option[value=fullColour]").remove();
    $("#colourSleeveSelectDropDown").find("option[value=fullColour]").remove();
  }
}

function createEditOrderSummaryCallback(detailNumber) {
  return function() {
    //set the current Id from LocalStorage
    setCurrentDetailNumber(detailNumber);
    //will data bind the form with the correct data based on the above detailNumber
    //need to clear detailStage1_form and detailStage4_form
    //then bind their values again here
    clearDetailStage1();
    clearDetailStage4();
    bindDetail1Data();
    bindDetail4Data();
    updateBodyClassStage(1);
    //ideally call the price handler in here somehow!
    moveOntoNextSection("#detailStage8", "#detailStage1");
    //need to clear the form here of all data
  };
}

function showHideNoOrderSummary(noOrders) {
  if (noOrders) {
    $("#noOrdersSummary").show();
    $("#orderSummaryFull").hide();
  } else {
    $("#noOrdersSummary").hide();
    $("#orderSummaryFull").show();
  }
}

function clearLocalStorageOrderDetailValues(detailNumber) {
  localStorage.setItem("detailStage1_" + detailNumber, "");
  localStorage.setItem("detailStage2_" + detailNumber, "");
  localStorage.setItem("detailStage3_" + detailNumber, "");
  localStorage.setItem("detailStage4_" + detailNumber, "");
  localStorage.setItem("detailStage5_" + detailNumber, "");
  localStorage.setItem("detailStage1_form_" + detailNumber, "");
  localStorage.setItem("detailStage4_form_" + detailNumber, "");
}

function deleteSummaryItem(detailNumber) {
  return function() {
    $("#orderSummaryUlOuter").empty();
    var totalNumberOfItems = getTotalNumberOfOrderDetails();
    setTotalNumberOfOrderDetails(totalNumberOfItems - 1);
    clearLocalStorageOrderDetailValues(detailNumber);
    if (getTotalNumberOfOrderDetails() > 0) {
      bindOrderSummaryDetails();
      showHideNoOrderSummary(false);
    } else {
      bindOrderSummaryDetails();
      showHideNoOrderSummary(true);
    }
  };
}

function cancelDeleteSummaryItem(detailNumber) {
  return function() {
    $("#confirmDeleteDiv" + detailNumber).hide();
  };
}

function createDeleteOrderSummaryCallback(detailNumber) {
  return function() {
    $("#confirmDeleteDiv" + detailNumber).show();
  };
}

function bindOrderSummaryDetailsAddGarment() {
  var summaryId = "#orderSummaryUlOuterAddGarment";

  var summaryObj = {};

  var totalNumberOfOrders = getTotalNumberOfOrderDetails();
  $(summaryId).empty();

  var runningTotal = 0;
  var data = [];
  var totalNumberOfDetails = 0;
  //get the total number of items in local storage, including the ones that have been deleted
  for (var i = 0; i < localStorage.length; i++) {
    var key = localStorage.key(i);
    if (key.search("detailStage1_form_") > -1) {
      totalNumberOfDetails++;
    }
  }

  //now we only wnt to bind the ones that have data
  for (var i = 1; i <= totalNumberOfDetails; i++) {
    var jsonObj = {};
    var data = localStorage.getItem("detailStage1_form_" + i);
    var priceData = localStorage.getItem("detailStage4_form_" + i);
    if (data && priceData) {
      jsonObj = JSON.parse(data);
      priceObj = JSON.parse(priceData);
      var interimPrice = parseFloat(priceObj.totalPrice) || 0;
      runningTotal += interimPrice;
      var liSummaryItem = $("<li></li>").addClass("summary__item");
      var ulSummaryRow = $("<ul></ul>").addClass("summary__row");
      liSummaryItem.append(ulSummaryRow);

      var liSummaryItemQuanity = $("<li></li>");
      liSummaryItemQuanity.text(jsonObj.quantity);
      liSummaryItemQuanity.addClass("summary_item__quantity");
      ulSummaryRow.append(liSummaryItemQuanity);
      var liSummaryItemDescription = $("<li></li>");
      var summaryDescriptionText = buildOrderSummaryDescriptionText(
        jsonObj,
        priceObj
      );
      liSummaryItemDescription.text(summaryDescriptionText);
      liSummaryItemDescription.addClass("summary-item__description");
      ulSummaryRow.append(liSummaryItemDescription);
      var liSummaryPrice = $("<li></li>");
      liSummaryPrice.text("£" + priceObj.totalPrice);
      liSummaryPrice.addClass("summary-item__total total-price");
      ulSummaryRow.append(liSummaryPrice);
      var divSummaryEdit = $("<div></div>");
      divSummaryEdit.addClass("summary__edit");

      $(summaryId).append(liSummaryItem);

      summaryObj.orderTitle += jsonObj.garmentType + " ";
      summaryObj.orderDescription += summaryDescriptionText + " ";
    } //close iff
  } //close for
  $("#totalSummary").text("£" + runningTotal.toFixed(2));
}

function bindOrderSummaryDetails() {
  var summaryId = "#orderSummaryUlOuter";

  var summaryObj = {};

  var totalNumberOfOrders = getTotalNumberOfOrderDetails();
  $(summaryId).empty();

  var runningTotal = 0;
  var data = [];
  var totalNumberOfDetails = 0;
  //get the total number of items in local storage, including the ones that have been deleted
  for (var i = 0; i < localStorage.length; i++) {
    var key = localStorage.key(i);
    if (key.search("detailStage1_form_") > -1) {
      totalNumberOfDetails++;
    }
  }
  //now we only wnt to bind the ones that have data
  for (var i = 1; i <= totalNumberOfDetails; i++) {
    var jsonObj = {};
    var data = localStorage.getItem("detailStage1_form_" + i);
    var priceData = localStorage.getItem("detailStage4_form_" + i);
    
    if (data && priceData) {
      jsonObj = JSON.parse(data);
      priceObj = JSON.parse(priceData);
      var interimPrice = parseFloat(priceObj.totalPrice) || 0;
      runningTotal += interimPrice;
      var liSummaryItem = $("<li></li>").addClass("summary__item");
      var ulSummaryRow = $("<ul></ul>").addClass("summary__row");
      liSummaryItem.append(ulSummaryRow);

      var liSummaryItemQuanity = $("<li></li>");
      liSummaryItemQuanity.text(jsonObj.quantity);
      liSummaryItemQuanity.addClass("summary_item__quantity");
      ulSummaryRow.append(liSummaryItemQuanity);
      var liSummaryItemDescription = $("<li></li>");
      var summaryDescriptionText = buildOrderSummaryDescriptionText(
        jsonObj,
        priceObj
      );
      liSummaryItemDescription.text(summaryDescriptionText);
      liSummaryItemDescription.addClass("summary-item__description");
      ulSummaryRow.append(liSummaryItemDescription);
      var liSummaryPrice = $("<li></li>");
      liSummaryPrice.text("£" + priceObj.totalPrice);
      liSummaryPrice.addClass("summary-item__total total-price");
      ulSummaryRow.append(liSummaryPrice);
      var divSummaryEdit = $("<div></div>");
      divSummaryEdit.addClass("summary__edit");

      var editLink = $("<a />", {
        text: " Edit Item ",
        on: {
          click: createEditOrderSummaryCallback(i)
        }
      });
      editLink.addClass("btn--hollow");
      divSummaryEdit.append(editLink);
      var deleteLink = $("<a />", {
        text: " Bin",
        on: {
          click: createDeleteOrderSummaryCallback(i)
        }
      });
      deleteLink.addClass("btn--delete");

      var divConfirmDelete = $("<div></div>").attr(
        "id",
        "confirmDeleteDiv" + i
      );
      divConfirmDelete.addClass("delete-confirmation");

      var deleteTextBlock = $("<p></p>", {
        text: "Are you sure? Orders cannot be retrived once deleted!"
      });
      deleteTextBlock.addClass("longform");

      var deleteLinkActual = $("<span></span>", {
        text: "Yes",
        on: {
          click: deleteSummaryItem(i)
        }
      });
      deleteLinkActual.addClass("delete-confirmation--yes");

      var cancelDeleteLink = $("<span></span>", {
        text: "No",
        on: {
          click: cancelDeleteSummaryItem(i)
        }
      });
      cancelDeleteLink.addClass("delete-confirmation--no");

      divConfirmDelete.append(deleteTextBlock);

      deleteTextBlock.append(cancelDeleteLink);
      deleteTextBlock.append(deleteLinkActual);

      divConfirmDelete.hide();

      divSummaryEdit.append(deleteLink);
      liSummaryItem.append(divSummaryEdit);

      liSummaryItem.append(divConfirmDelete);

      $(summaryId).append(liSummaryItem);

      summaryObj.orderTitle += jsonObj.garmentType + " ";
      summaryObj.orderDescription += summaryDescriptionText + " ";
    } //close if
  } //close for
  $("#totalSummary").text("£" + runningTotal.toFixed(2));

  summaryObj.amount = runningTotal;
  summaryObj.email = localStorage["emailAddress"];

  localStorage["orderSummary"] = JSON.stringify(summaryObj);

  var hideOrderSummary = totalNumberOfDetails === 0;
  showHideNoOrderSummary(hideOrderSummary);
} //close method

function buildOrderSummaryDescriptionText(jsonObj, priceObj) {
  var frontColourText = "";

  if (priceObj.colourFrontTotal && priceObj.colourFrontTotal > 0) {
    var colourText = "";
    for (var i = 1; i <= priceObj.colourFrontTotal; i++) {
      colourText = colourText + priceObj["colourFront" + i] + ", ";
    }
    var colourTextFormatted = colourText.replace(/,\s*$/, "");
    frontColourText =
      "Colours: " +
      priceObj.colourFrontTotal +
      " On: Front (" +
      colourTextFormatted +
      ") ";
  } else if (priceObj.colourFrontTotal === "fullColour") {
    // full colour
    frontColourText = " Colours: Full Colour On: Front ";
  }

  var backColourText = "";
  if (priceObj.colourBackTotal && priceObj.colourBackTotal > 0) {
    var colourText = "";
    for (var i = 1; i <= priceObj.colourBackTotal; i++) {
      colourText = colourText + priceObj["colourBack" + i] + ", ";
    }
    var colourTextFormatted = colourText.replace(/,\s*$/, "");
    backColourText =
      "Colours: " +
      priceObj.colourBackTotal +
      " On: Back (" +
      colourTextFormatted +
      ") ";
  } else if (priceObj.colourBackTotal === "fullColour") {
    // full colour
    backColourText = " Colours: Full Colour On: Back ";
  }

  var sleeveColourText = "";
  if (priceObj.colourSleeveTotal && priceObj.colourSleeveTotal > 0) {
    var colourText = "";
    for (var i = 1; i <= priceObj.colourSleeveTotal; i++) {
      colourText = colourText + priceObj["colourSleeve" + i] + ", ";
    }
    var colourTextFormatted = colourText.replace(/,\s*$/, "");
    sleeveColourText =
      "Colours: " +
      priceObj.colourSleeveTotal +
      " On: Sleeve (" +
      colourTextFormatted +
      ") ";
  } else if (priceObj.colourSleeveTotal === "fullColour") {
    // full colour
    sleeveColourText = " Colours: Full Colour On: Sleeve ";
  }

  var text =
    jsonObj.quality +
    " " +
    jsonObj.garmentType +
    " (" +
    jsonObj.colour +
    ")" +
    " " +
    frontColourText +
    backColourText +
    sleeveColourText;

  return text;
}

function storeDetail1Data() {
  var dataObj = {};
  dataObj.garmentType = $("#garmentTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");
  dataObj.quality = $("#qualityTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");

  //gets the input that is visible on the page with the input in...
  var element = $("#colourArea").find(".js-show").find("input");
  dataObj.selectedColourId = element.attr("id");
  dataObj.selectedColour = $("#colourArea")
    .find(".js-show")
    .attr("selected-value");
  var selectedColour = $("#" + dataObj.selectedColourId).attr("selected-value");
  if (selectedColour) {
    dataObj.colour = selectedColour;
  } else {
    dataObj.colour = "White";
  }
  dataObj.quantitySelected = getSelectedQuantity();
  dataObj.quantity = getSelectedQuantityAllItems();
  dataObj.sizeSmall = $("#sizeSmall").val();
  dataObj.sizeMedium = $("#sizeMedium").val();
  dataObj.sizeLarge = $("#sizeLarge").val();
  dataObj.sizeXLarge = $("#sizeXLarge").val();
  dataObj.size2XLarge = $("#size2XLarge").val();
  var localStorageKey = "detailStage1_form" + "_" + getCurrentDetailNumber();
  localStorage[localStorageKey] = JSON.stringify(dataObj);

  storeCurrentGarnmentColour(dataObj.colour);
}
function storeDetail4Data() {
  var dataObj = {};
  //drop down list values
  dataObj.colourFrontTotal = $("#colourFront").val();

  if (dataObj.colourFrontTotal === "4") {
    dataObj.colourFront1 = $("#inkcolor-front-1").attr("selected-value");
    dataObj.colourFront2 = $("#inkcolor-front-2").attr("selected-value");
    dataObj.colourFront3 = $("#inkcolor-front-3").attr("selected-value");
    dataObj.colourFront4 = $("#inkcolor-front-4").attr("selected-value");
  } else if (dataObj.colourFrontTotal === "3") {
    dataObj.colourFront1 = $("#inkcolor-front-1").attr("selected-value");
    dataObj.colourFront2 = $("#inkcolor-front-2").attr("selected-value");
    dataObj.colourFront3 = $("#inkcolor-front-3").attr("selected-value");
  } else if (dataObj.colourFrontTotal === "2") {
    dataObj.colourFront1 = $("#inkcolor-front-1").attr("selected-value");
    dataObj.colourFront2 = $("#inkcolor-front-2").attr("selected-value");
  } else if (dataObj.colourFrontTotal === "1") {
    dataObj.colourFront1 = $("#inkcolor-front-1").attr("selected-value");
  }
  dataObj.colourBackTotal = $("#colourBack").val();
  if (dataObj.colourBackTotal === "4") {
    dataObj.colourBack1 = $("#inkcolor-back-1").attr("selected-value");
    dataObj.colourBack2 = $("#inkcolor-back-2").attr("selected-value");
    dataObj.colourBack3 = $("#inkcolor-back-3").attr("selected-value");
    dataObj.colourBack4 = $("#inkcolor-back-4").attr("selected-value");
  } else if (dataObj.colourBackTotal === "3") {
    dataObj.colourBack1 = $("#inkcolor-back-1").attr("selected-value");
    dataObj.colourBack2 = $("#inkcolor-back-2").attr("selected-value");
    dataObj.colourBack3 = $("#inkcolor-back-3").attr("selected-value");
  } else if (dataObj.colourBackTotal === "2") {
    dataObj.colourBack1 = $("#inkcolor-back-1").attr("selected-value");
    dataObj.colourBack2 = $("#inkcolor-back-2").attr("selected-value");
  } else if (dataObj.colourBackTotal === "1") {
    dataObj.colourBack1 = $("#inkcolor-back-1").attr("selected-value");
  }
  dataObj.colourSleeveTotal = $("#colourSleeveSelectDropDown").val();
  if (dataObj.colourSleeveTotal === "4") {
    dataObj.colourSleeve1 = $("#inkcolor-sleeve-1").attr("selected-value");
    dataObj.colourSleeve2 = $("#inkcolor-sleeve-2").attr("selected-value");
    dataObj.colourSleeve3 = $("#inkcolor-sleeve-3").attr("selected-value");
    dataObj.colourSleeve4 = $("#inkcolor-sleeve-4").attr("selected-value");
  } else if (dataObj.colourSleeveTotal === "3") {
    dataObj.colourSleeve1 = $("#inkcolor-sleeve-1").attr("selected-value");
    dataObj.colourSleeve2 = $("#inkcolor-sleeve-2").attr("selected-value");
    dataObj.colourSleeve3 = $("#inkcolor-sleeve-3").attr("selected-value");
  } else if (dataObj.colourSleeveTotal === "2") {
    dataObj.colourSleeve1 = $("#inkcolor-sleeve-1").attr("selected-value");
    dataObj.colourSleeve2 = $("#inkcolor-sleeve-2").attr("selected-value");
  } else if (dataObj.colourSleeveTotal === "1") {
    dataObj.colourSleeve1 = $("#inkcolor-sleeve-1").attr("selected-value");
  }

  dataObj.totalPrice = totalUpDetailPrice();
  var localStorageKey = "detailStage4_form" + "_" + getCurrentDetailNumber();
  var localStoragePriceKey =
    "detailStage4_form_price" + "_" + getCurrentDetailNumber();
  localStorage[localStorageKey] = JSON.stringify(dataObj);
  localStorage[localStoragePriceKey] = dataObj.totalPrice;

  storeRunningPriceTotal(dataObj.totalPrice);
}

function bindDetail1Data() {
  var localStorageKey = "detailStage1_form" + "_" + getCurrentDetailNumber();
  var localStorageData = localStorage.getItem(localStorageKey);
  if (localStorageData !== null && localStorageData) {
    var dataObj = JSON.parse(localStorageData);
    $("#sizeSmall").val(dataObj.sizeSmall);
    $("#sizeMedium").val(dataObj.sizeMedium);
    $("#sizeLarge").val(dataObj.sizeLarge);
    $("#sizeXLarge").val(dataObj.sizeXLarge);
    $("#size2XLarge").val(dataObj.size2XLarge);

    var gtKey = '[data-value="' + dataObj.garmentType + '"]';
    $("#garmentTypeSelector").find(gtKey).addClass("select_box--checked");

    var qualityKey = '[data-value="' + dataObj.quality + '"]';
    $("#qualityTypeSelector").find(qualityKey).addClass("select_box--checked");

    var quantityKey = '[value="' + dataObj.quantitySelected + '"]';
    $("#quantityTypeSelector")
      .find(quantityKey)
      .addClass("select_box--checked");

    var idOfColourPicker = dataObj.selectedColourId;
    var formattedId = "#" + idOfColourPicker;
    $(formattedId).val(dataObj.colour);
    $(formattedId).data("paletteColorPickerPlugin").reload();
    $("." + idOfColourPicker).addClass("js-show");
    $("#colourArea").addClass("js-show");
    totalUpSizes();
  }
}

function bindDetail4Data() {
  clearDetailStage4(); //clear down the values first of all, before we bind them
  //need to set up the drop downs so that they have the correct values if we select full colour
  loadColourDropDownValues();
  var localStorageKey = "detailStage4_form" + "_" + getCurrentDetailNumber();
  var localStorageData = localStorage.getItem(localStorageKey);
  if (localStorageData && localStorageData !== null) {
    var dataObj = JSON.parse(localStorageData);
    $("#colourFront").val(dataObj.colourFrontTotal).change();
    if (dataObj.colourFront1) {
      $("#inkcolor-front-1").val(dataObj.colourFront1);
      $("#inkcolor-front-1").data("paletteColorPickerPlugin").reload();
    }
    if (dataObj.colourFront2) {
      $("#inkcolor-front-2").val(dataObj.colourFront2);
      $("#inkcolor-front-2").data("paletteColorPickerPlugin").reload();
    }
    if (dataObj.colourFront3) {
      $("#inkcolor-front-3").val(dataObj.colourFront3);
      $("#inkcolor-front-3").data("paletteColorPickerPlugin").reload();
    }
    if (dataObj.colourFront4) {
      $("#inkcolor-front-4").val(dataObj.colourFront4);
      $("#inkcolor-front-4").data("paletteColorPickerPlugin").reload();
    }

    $("#colourBack").val(dataObj.colourBackTotal);
    if (dataObj.colourBack1) {
      $("#inkcolor-back-1").val(dataObj.colourBack1);
      $("#inkcolor-back-1").data("paletteColorPickerPlugin").reload();
    }
    if (dataObj.colourBack2) {
      $("#inkcolor-back-2").val(dataObj.colourBack2);
      $("#inkcolor-back-2").data("paletteColorPickerPlugin").reload();
    }
    if (dataObj.colourBack3) {
      $("#inkcolor-back-3").val(dataObj.colourBack3);
      $("#inkcolor-back-3").data("paletteColorPickerPlugin").reload();
    }
    if (dataObj.colourBack4) {
      $("#inkcolor-back-4").val(dataObj.colourBack4);
      $("#inkcolor-back-4").data("paletteColorPickerPlugin").reload();
    }
    $("#colourSleeveSelectDropDown").val(dataObj.colourSleeveTotal);
    if (dataObj.colourSleeve1) {
      $("#inkcolor-sleeve-1").val(dataObj.colourSleeve1);
      $("#inkcolor-sleeve-1").data("paletteColorPickerPlugin").reload();
    }
    if (dataObj.colourSleeve2) {
      $("#inkcolor-sleeve-2").val(dataObj.colourSleeve2);
      $("#inkcolor-sleeve-2").data("paletteColorPickerPlugin").reload();
    }
    if (dataObj.colourSleeve3) {
      $("#inkcolor-sleeve-3").val(dataObj.colourSleeve3);
      $("#inkcolor-sleeve-3").data("paletteColorPickerPlugin").reload();
    }
    if (dataObj.colourSleeve4) {
      $("#inkcolor-sleeve-4").val(dataObj.colourSleeve4);
      $("#inkcolor-sleeve-4").data("paletteColorPickerPlugin").reload();
    }
    var totalPriceForThisItem = calculateTotalPrice();
    var safeTotalPrice = totalPriceForThisItem / 100;

    storeRunningPriceTotal(safeTotalPrice);
  }
}

function showImageUploadImagesFront() {
  //hide them all first
  $("#uploadRowFrontA").hide();
  $("#uploadRowFrontB").hide();
  $("#uploadRowFrontC").hide();
  $("#uploadRowFrontD").hide();
  $("#uploadRowFrontE").hide();
  $("#uploadRowFrontF").hide();
  $("#uploadRowFrontG").hide();
  $("#uploadRowFrontH").hide();
  $("#uploadRowFrontI").hide();
  var garmentImageDiv = $("#garmentImageFront");
  garmentImageDiv.removeClass();
  garmentImageDiv.addClass("one-half");

  var garmentType = $("#garmentTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");

  if (garmentType === "Unisex T-Shirt") {
    garmentImageDiv.addClass("garment-img-uni-front");
    bindFrontFileUploadTextStandard();
    // 
    $("#uploadRowFrontA").show();
    $("#uploadRowFrontB").show();
    $("#uploadRowFrontC").show();
    $("#uploadRowFrontD").show();
    $("#uploadRowFrontE").show();
    $("#uploadRowFrontF").show();
    $("#uploadRowFrontG").show();
  } else if (garmentType === "Ladies T-Shirt") {
    garmentImageDiv.addClass("garment-img-lad-front");

    bindFrontFileUploadTextStandard();
    $("#uploadRowFrontA").show();
    $("#uploadRowFrontB").show();
    $("#uploadRowFrontC").show();
    $("#uploadRowFrontD").show();
    $("#uploadRowFrontE").show();
    $("#uploadRowFrontF").show();
    $("#uploadRowFrontG").show();
  } else if (garmentType === "Kids T-Shirt") {
    garmentImageDiv.addClass("garment-img-kid-front");
    bindFrontFileUploadHandlers();
    bindFrontFileUploadTextStandard();
    $("#uploadRowFrontA").show();
    $("#uploadRowFrontB").show();
    $("#uploadRowFrontC").show();
    $("#uploadRowFrontD").show();
    $("#uploadRowFrontE").show();
    $("#uploadRowFrontF").show();
    $("#uploadRowFrontG").show();
  } else if (garmentType === "Long Sleeve T-Shirt") {
    garmentImageDiv.addClass("garment-img-long-front");
    bindFrontFileUploadHandlers();
    bindFrontFileUploadTextStandard();
    $("#uploadRowFrontA").show();
    $("#uploadRowFrontB").show();
    $("#uploadRowFrontC").show();
    $("#uploadRowFrontD").show();
    $("#uploadRowFrontE").show();
    $("#uploadRowFrontF").show();
    $("#uploadRowFrontG").show();
    $("#uploadRowFrontH").show();
    $("#uploadRowFrontI").show();
  } else if (garmentType === "Sweat Shirt") {
    garmentImageDiv.addClass("garment-img-swe-front");
    bindFrontFileUploadTextStandard();
    $("#uploadRowFrontA").show();
    $("#uploadRowFrontB").show();
    $("#uploadRowFrontC").show();
    $("#uploadRowFrontD").show();
    $("#uploadRowFrontE").show();
    $("#uploadRowFrontF").show();
    $("#uploadRowFrontG").show();
    $("#uploadRowFrontH").show();
    $("#uploadRowFrontI").show();
  } else if (garmentType === "Hooded Top") {
    garmentImageDiv.addClass("garment-img-hood-front");
    bindFrontFileUploadTextHoodie();
    $("#uploadRowFrontA").show();
    $("#uploadRowFrontB").show();
    $("#uploadRowFrontC").show();
    $("#uploadRowFrontD").show();
    $("#uploadRowFrontE").show();
    $("#uploadRowFrontF").show();
    $("#uploadRowFrontG").show();
    $("#uploadRowFrontH").show();
  } else if (garmentType === "Zipped Hoodies") {
    garmentImageDiv.addClass("garment-img-zip-front");
    bindFrontFileUploadTextZipped();
    $("#uploadRowFrontA").show();
    $("#uploadRowFrontB").show();
    $("#uploadRowFrontC").show();
    $("#uploadRowFrontD").show();
    $("#uploadRowFrontE").show();
    $("#uploadRowFrontF").show();
  }
}

function showImageUploadImagesBack() {
  var garmentImageDiv = $("#garmentImageBack");
  garmentImageDiv.removeClass();
  garmentImageDiv.addClass("one-half");

  var garmentType = $("#garmentTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");

  var garmentImage = $("#garmentImageBack");

  if (garmentType === "Unisex T-Shirt") {
    garmentImage.addClass("garment-img-uni-back ");
  } else if (garmentType === "Ladies T-Shirt") {
    garmentImage.addClass("garment-img-lad-back");
  } else if (garmentType === "Kids T-Shirt") {
    garmentImage.addClass("garment-img-kid-back");
  } else if (garmentType === "Long Sleeve T-Shirt") {
    garmentImage.addClass("garment-img-long-back");
  } else if (garmentType === "Sweat Shirt") {
    garmentImage.addClass("garment-img-swe-back");
  } else if (garmentType === "Hooded Top") {
    garmentImage.addClass("garment-img-hood-back");
  } else if (garmentType === "Zipped Hoodies") {
    garmentImage.addClass("garment-img-zip-back");
  }
}

/* 
    FORM SUBMISSION - FORWARDS
*/

function moveOntoNextSection(stageToHide, stageToShow) {
  storeFormData(stageToHide);
  showNextSection(stageToHide, stageToShow);
  dataBindSection(stageToShow);
  bindDetail1Data();
}

function showNextSection(stageToHide, stageToShow) {
  $("html, body").animate({ scrollTop: 0 });
  $(stageToHide).fadeOut(10);
  $(stageToShow).fadeIn(150);

  // if ($(stageToShow).hasClass("html.no-smil")) {
  //   $(stageToShow).css({ visibility: "visible" });
  // }
}

function validateFormContents(rules, formId) {
  var form = document.getElementById(formId);
  var validStatus = rsv.validate(form, rules);
  return validStatus;
}

function updateBodyClassStage(stageNumber) {
  $("#js-body").removeClass();
  $("#js-body").addClass("js-stage" + stageNumber);
}

/**
 * @version 1.1
 * personal details
 * 
 * CA notes:
 * @since 1.1 This section was originally stage 1, this has now been moved to stage 7 (before delivery)
 */
function orderdetailStage7Submit() {
  var rules = buildDetailStage6ValidationRules();
  var validStatus = validateFormContents(rules, "detailStage72_form");

  if (validStatus) {
    moveOntoNextSection("#detailStage72", "#detailStage1");
    totalUpPricesWithColour();
    updateCareDetailsLink();
    updateBodyClassStage(2);
    localStorage["emailAddress"] = $("#emailAddress").val();
  }
  return false;
}

/**
 * Order Sub Details (size and item etc.)
 */
function odStage1Submit() {
  var rules = buildDetailStage1ValidationRules();
  var validStatus = validateFormContents(rules, "detailStage1_form");
  if (validStatus) {
    storeDetail1Data();
    clearDetailStage2();
    totalUpPricesWithColour();
    showImageUploadImagesFront();
    showNextSection("#detailStage1", "#detailStage2");
    dataBindSection("#detailStage2");
    updateBodyClassStage(2);
    localStorage["returningUser"] = true;
  }
  return false;
}

/**
 * front files
 */
function odStage2Submit() {
  moveOntoNextSection("#detailStage2", "#detailStage3");
  updateBodyClassStage(3);
  totalUpPricesWithColour();
  showImageUploadImagesBack();
  bindBackFileUploadHandlers();
  bindBackFileUploadText();
  return false;
}

/**
 * Back files
 */
function odStage3Submit() {
  loadColourDropDownValues();
  showNextSection("#detailStage3", "#detailStage4");
  //moveOntoNextSection("#detailStage3", "#detailStage4");
  bindDetail4Data();
  totalUpPricesWithColour();
  colourSleeveSelectShowHide();
  colourFrontSelectShowHide();
  colourBackSelectShowHide();
  updateBodyClassStage(4);
  return false;
}

/**
 * Colours etc.
 */
function odStage4Submit() {
  var rules = buildDetailStage4ValidationRules();
  var validStatus = validateFormContents(rules, "detailStage4_form");
  if (validStatus) {
    storeDetail4Data();
    totalUpPricesWithColour();
    showNextSection("#detailStage4", "#detailStage5");
    dataBindSection("#detailStage5");
    updateBodyClassStage(5);

    bindOrderSummaryDetailsAddGarment();
  }
  return false;
}

/**
 * Other information on the order
 */
function odStage5Submit() {
dataBindSection("#detailStage6");  
  moveOntoNextSection("#detailStage5", "#detailStage6");
  //TODO - may need to tweak this if we are in edit mode...
  // incrementTotalDetailNumber(true);
  // incrementCurrentDetailNumber(true);
  //need some check to find the total number of orders, and then increment it...
  populateAddressDetails();
  updateBodyClassStage(6);
  return false;
}

/**
 * Address Details
 */
function odStage7Submit() {
  //POPULATE ORDER SUMMARY IN HERE
  var rules = buildHeaderStage2ValidationRules();
  var isValid = validateFormContents(rules, "detailStage7_form");

  if (isValid) {
    moveOntoNextSection("#detailStage7", "#detailStage8");
    var detailNumber = getCurrentDetailNumber();

    displayAddressSummaryDetails();
    bindOrderSummaryDetails();

    $("#deliveryDateSummary").text(GetDeliveryDate());
    $("#createAccountSummary").checked = $("#createAccount").checked;
    if ($("#createAccountSummary").checked) {
      //show the password area
      $("#passwordFormAreaSummary").show();
    } else {
      $("#passwordFormAreaSummary").hide();
    }
    updateBodyClassStage(8);
  }

  return false;
}

function getFormDataJson(localStorageKey) {
  var data = localStorage[localStorageKey];
  if (data) {
    var dataObj = JSON.parse(data);
    return dataObj;
  } else {
    return {}; //return dummy value for now...
  }
}

function odStage8Submit() {
  var submitObject = {};
  submitObject["detailStage6"] = getFormDataJson("#detailStage6");
  submitObject["detailStage7"] = getFormDataJson("#detailStage7");

  var totalNumberOfDetails = 0;
  //get the total number of items in local storage, including the ones that have been deleted
  for (var i = 0; i < localStorage.length; i++) {
    var key = localStorage.key(i);
    if (key.search("detailStage1_form_") > -1) {
      totalNumberOfDetails++;
    }
  }
  //now we only wnt to bind the ones that have data
  for (var i = 1; i <= totalNumberOfDetails; i++) {
    submitObject["detailStage1_" + i] = getFormDataJson(
      "detailStage1_form_" + i
    );
    submitObject["detailStage2_" + i] = getFormDataJson("#detailStage2_" + i);

    submitObject["detailStage4_" + i] = getFormDataJson(
      "detailStage4_form_" + i
    );
    submitObject["detailStage5_" + i] = getFormDataJson("#detailStage5_" + i);
    submitObject["detailStage2_" + i] = getFormDataJson("#detailStage2_" + i);
    submitObject["detailStage6_" + i] = getFormDataJson("#detailStage6_" + i);
    submitObject["detailStage7_" + i] = getFormDataJson("#detailStage7_" + i);
  }

  submitObject["totalNumberOfDetails"] = totalNumberOfDetails;

  var summaryObjStr = localStorage["orderSummary"];
  var summaryObj = JSON.parse(summaryObjStr);

  summaryObj.orderTitle = "Your T-Shirt Order";
  summaryObj.orderDescription = "Your T-Shirt Order";

  SUBMIT_OBJECT = submitObject;

  handleStripePaymentIntegration(summaryObj);

  if (PAYMENT_SUCCESS) {
  }
  return false;
}

/* 
    FORM SUBMISSION - BACKWARDS
*/
function odStage1Back() {
  if (getCurrentDetailNumber() === 1) {
    moveOntoNextSection("#detailStage1", "#detailStage2");
  } else {
    //we probably want to move back to the "add garment page", and decrease the current step number to reflect the curren status
    incrementCurrentDetailNumber();
    moveOntoNextSection("#detailStage1", "#detailStage5");
    updateBodyClassStage(1);
  }
  return false;
}

function odStage2Back() {
  //store the form data for this section
  //databind with the custom data
  bindDetail1Data();
  //move back as normal
  bindFrontFileUploadHandlers();
  showImageUploadImagesFront();
  showNextSection("#detailStage2", "#detailStage1");
  updateBodyClassStage(1);
  return false;
}

function odStage3Back() {
  showImageUploadImagesFront();
  moveOntoNextSection("#detailStage3", "#detailStage2");
  updateBodyClassStage(2);
  return false;
}

function odStage4Back() {
  showImageUploadImagesBack();
  storeDetail4Data();
  showNextSection("#detailStage4", "#detailStage3");
  //moveOntoNextSection("#detailStage4", "#detailStage3");
  updateBodyClassStage(3);
  return false;
}

function odStage5Back() {
  bindDetail4Data();
  showNextSection("#detailStage5", "#detailStage4");
  colourSleeveSelectShowHide();
  colourFrontSelectShowHide();
  colourBackSelectShowHide();
  updateBodyClassStage(4);
  return false;
}

/**
 * @version 1.1
 * @author Christopher Atwood
 * @since 1.1 Added event handler to handle back action.
 */
function odStage6Back() {
    bindDetail4Data();
    showNextSection("#detailStage6", "#detailStage5");
    colourSleeveSelectShowHide();
    colourFrontSelectShowHide();
    colourBackSelectShowHide();
    updateBodyClassStage(5);

    return false;
}
/**
 * @version 1.1
 * @author Christopher Atwood
 * @since 1.1 Added function to handle event handler.
 */
function odStage6Submit() {
    var rules = buildDetailStage6ValidationRules();
    var validStatus = validateFormContents(rules, "detailStage6_form");

    if (validStatus) {
        moveOntoNextSection("#detailStage6", "#detailStage7");
        totalUpPricesWithColour();
        updateCareDetailsLink();
        updateBodyClassStage(7);
        localStorage["returningUser"] = true;
        localStorage["emailAddress"] = $("#emailAddress").val();
    }
    return false;
}

function odStage7Back() {
  moveOntoNextSection("#detailStage7", "#detailStage5");
  updateBodyClassStage(6);
  bindOrderSummaryDetailsAddGarment();
  return false;
}

function odStage8Back() {
  moveOntoNextSection("#detailStage8", "#detailStage7");
  updateBodyClassStage(7);
  return false;
}

/*
    FORM RESET
*/
function clearForm(formName) {
  cleardetailStage7();
  clearDetailStages();
  clearHeaderStage2();
  clearHeaderStage3();
}

function clearDetailStages() {
  clearDetailStage1();
  clearDetailStage2();
  clearDetailStage3();
  clearDetailStage4();
  clearDetailStage5();
}

function cleardetailStage7() {
  $("#firstName").val("");
  $("#phoneNumber").val("");
  $("#emailAddress").val("");
  $("#password").val("");
  $("#createAccount").attr("checked", false);
  $("#passwordFormArea").hide();
}
function clearHeaderStage2() {
  $("#addressLine1Delivery").val("");
  $("#addressLine2Delivery").val("");
  $("#addressLine3Delivery").val("");
  $("#cityDelivery").val("");
  $("#postCodeDelivery").val("");

  $("#addressLine1Billing").val("");
  $("#addressLine2Billing").val("");
  $("#addressLine3Billing").val("");
  $("#cityBilling").val("");
  $("#postCodeBilling").val("");
}
function clearHeaderStage3() {
  //reset all of the form fields to be blank

  $("#addressLine1Summary").val("");
  $("#addressLine2Summary").val("");
  $("#addressLine3Summary").val("");
  $("#citySummary").val("");
  $("#postCodeSummary").val("");

  $("#addressLine1DeliverySummary").val("");
  $("#addressLine2DeliverySummary").val("");
  $("#addressLine3DeliverySummary").val("");
  $("#cityDeliverySummary").val("");
  $("#postCodeDeliverySummary").val("");
  $("#password").val("");
  $("#createAccountSummary").attr("checked", false);
  $("#passwordFormArea").hide();
}

function clearDetailStage1() {
  var cssToRemove = "select_box--checked";
  $("#detailStage1 a").removeClass(cssToRemove);
  $("#sizesArea").find("input").each(function(index, value) {
    $(this).val("");
  });

  $("#colourArea").find("input").each(function(index, value) {
    $(this).data("paletteColorPickerPlugin").reset();
    $(this).attr("selected-value", "");
  });

  $("#sizes-validation").removeClass("sizes-validation--tick");
  $("#sizes-validation").text("0");
  $("#infoArea").hide();
}

function resetFileUploadBox(name, uploadObj) {
  var uploadObj = $(name).uploadFile(uploadObj);
  uploadObj.reset();

  var outerLi = $(name).parent();
  outerLi.children(".ajax-file-upload-container").empty();
}

function clearDetailStage2() {
  resetFileUploadBox("#fileUploadFrontPart1", handleFileUploadFront1);
  resetFileUploadBox("#fileUploadFrontPart2", handleFileUploadFront2);
  resetFileUploadBox("#fileUploadFrontPart3", handleFileUploadFront3);
  resetFileUploadBox("#fileUploadFrontPart4", handleFileUploadFront4);
  resetFileUploadBox("#fileUploadFrontPart5", handleFileUploadFront5);
  resetFileUploadBox("#fileUploadFrontPart6", handleFileUploadFront6);
  resetFileUploadBox("#fileUploadFrontPart7", handleFileUploadFront7);
  resetFileUploadBox("#fileUploadFrontPart8", handleFileUploadFront8);
  resetFileUploadBox("#fileUploadFrontPart9", handleFileUploadFront9);
}
function clearDetailStage3() {
  resetFileUploadBox("#fileUploadBackPartA", handleFileUploadBackA);
  resetFileUploadBox("#fileUploadBackPartB", handleFileUploadBackB);
  resetFileUploadBox("#fileUploadBackPartC", handleFileUploadBackC);
  // resetFileUploadBox("#fileUploadBackPartJ", handleFileUploadBackJ);
  // resetFileUploadBox("#fileUploadBackPartK", handleFileUploadBackK);
  // resetFileUploadBox("#fileUploadBackPartL", handleFileUploadBackL);
}
function clearDetailStage4() {
  //need to de-select the drop down lists
  //reset the colour pickers
  //remove their selected values
  $("#colourFront").val("0");
  $("#colourBack").val("0");
  $("#colourSleeveSelectDropDown").val("0");

  //hide the colour pickers
  $("#colourFrontSelect").hide();
  $("#colourBackSelect").hide();
  $("#colourSleeveSelect").hide();

  //reset the colour pickers
  $("#colourFrontSelect").find("input").each(function(index, value) {
    $(this).data("paletteColorPickerPlugin").clear();
    $(this).attr("selected-value", "");
  });
  $("#colourBackSelect").find("input").each(function(index, value) {
    $(this).data("paletteColorPickerPlugin").clear();
    $(this).attr("selected-value", "");
  });
  $("#colourSleeveSelect").find("input").each(function(index, value) {
    $(this).data("paletteColorPickerPlugin").clear();
    $(this).attr("selected-value", "");
  });
}
function clearDetailStage5() {
  $("#anyOtherNotes").val("");
}

function odStage5AddGarment() {
  $("#addAnotherGarmentPanel").show();
  $("#odStage5AddGarment").hide();
}

function addAnotherGarmentReuseFiles() {
  var currentDetailNumber = getCurrentDetailNumber();

  $.ajax({
    url: "reuse.php",
    type: "POST",
    data: { detail: currentDetailNumber + 1 },
    dataType: "json",
    success: function(data) {
      for (var i = 0; i < data.length; i++) {
        var path = data[i]["path"];
        var key = data[i]["key"];
        localStorage[key] = path;
      }
    }
  });

  addAnotherGarmentMoveToNextStage();
}

function addAnotherGarmentDontReUseFiles() {
  addAnotherGarmentMoveToNextStage();
}

function addAnotherGarmentMoveToNextStage() {
  incrementCurrentDetailNumber(true);
  incrementTotalDetailNumber(true);
  clearDetailStages();
  updateBodyClassStage(2);
  showNextSection("#detailStage5", "#detailStage1");
  $("#addAnotherGarmentPanel").hide();
  $("#odStage5AddGarment").show();
  return false;
}

function getCurrentDetailNumber() {
  var currentStatus = localStorage["currentDetailNumber"];
  if (currentStatus === undefined || currentStatus === null) {
    localStorage["currentDetailNumber"] = 1;
    return 1;
  } else {
    return parseInt(currentStatus);
  }
}

function setCurrentDetailNumber(val) {
  localStorage["currentDetailNumber"] = val;
}

function setTotalNumberOfOrderDetails(val) {
  localStorage["totalNumberOrderDetails"] = val;
}

function getTotalNumberOfOrderDetails() {
  var currentStatus = localStorage["totalNumberOrderDetails"];
  if (currentStatus === undefined || currentStatus === null) {
    localStorage["totalNumberOrderDetails"] = 1;
    return 1;
  } else {
    return parseInt(currentStatus);
  }
}

function incrementTotalDetailNumber(increment) {
  var currentStatus = localStorage["totalNumberOrderDetails"];
  if (currentStatus === undefined || currentStatus === null) {
    currentStatus = 1;
  }
  if (increment) {
    localStorage["totalNumberOrderDetails"] = parseInt(currentStatus) + 1;
  } else {
    localStorage["totalNumberOrderDetails"] = parseInt(currentStatus) - 1;
  }
}

function incrementCurrentDetailNumber(increment) {
  var currentStatus = localStorage["currentDetailNumber"];
  if (currentStatus === undefined || currentStatus === null) {
    currentStatus = 1;
  }
  if (increment) {
    localStorage["currentDetailNumber"] = parseInt(currentStatus) + 1;
  } else {
    localStorage["currentDetailNumber"] = parseInt(currentStatus) - 1;
  }
}

function orderHeaderStage8UpdateAddressSumbit() {
  var addressDetailsJson = getAddressDetails();

  addressDetailsJson.addressLine1Delivery = $(
    "#addressLine1DeliverySummary"
  ).val();
  addressDetailsJson.addressLine2Delivery = $(
    "#addressLine2DeliverySummary"
  ).val();
  addressDetailsJson.addressLine3Delivery = $(
    "#addressLine3DeliverySummary"
  ).val();
  addressDetailsJson.cityDeliverySummary = $("#cityDeliverySummary").val();
  addressDetailsJson.postCodeDelivery = $("#postCodeDeliverySummary").val();

  storeAddressDetails(addressDetailsJson);
  populateAddressDetailsForm(addressDetailsJson);
  displayAddressSummaryDetails();

  $("#addressOrderDetails").show();
  $("#addressOrderDetailsEdit").hide();

  return false;
}

function orderHeaderStage_8EditAddressSumbit() {
  displayAddressSummaryDetails();
  $("#addressOrderDetails").hide();
  $("#addressOrderDetailsEdit").show();
  return false;
}

function displayAddressSummaryDetails() {
  var addressDetailsJson = getAddressDetails();
  if (addressDetailsJson !== undefined && addressDetailsJson !== null) {
    //Textboxes for editing
    $("#addressLine1DeliverySummary").val(
      addressDetailsJson.addressLine1Delivery
    );
    $("#addressLine2DeliverySummary").val(
      addressDetailsJson.addressLine2Delivery
    );
    $("#addressLine3DeliverySummary").val(
      addressDetailsJson.addressLine3Delivery
    );
    $("#cityDeliverySummary").val(addressDetailsJson.cityDeliverySummary);
    $("#postCodeDeliverySummary").val(addressDetailsJson.postCodeDelivery);

    //labels for display
    $("#addressLine1Summary").text(addressDetailsJson.addressLine1Delivery);
    $("#addressLine2Summary").text(addressDetailsJson.addressLine2Delivery);
    $("#addressLine3Summary").text(addressDetailsJson.addressLine3Delivery);
    $("#addressCitySummary").text(addressDetailsJson.cityDeliverySummary);
    $("#addressPostCodeSummary").text(addressDetailsJson.postCodeDelivery);
  }
}

function orderHeaderStage8UpdateAddressCancel() {
  $("#addressOrderDetails").show();
  $("#addressOrderDetailsEdit").hide();
  return false;
}

function passwordShowHide() {
  if (this.checked) {
    $("#passwordFormArea").show(DEFAULT_SHOW_HIDE_SPEED);
  } else {
    $("#passwordFormArea").hide(DEFAULT_SHOW_HIDE_SPEED);
  }
}

function createPasswordSummaryChanged() {
  if (this.checked) {
    $("#passwordFormAreaSummary").show();
  } else {
    $("#passwordFormAreaSummary").hide();
  }
}

function useDeliveryAddress() {
  if (this.checked) {
    $("#addressLine1Billing").val($("#addressLine1Delivery").val());
    $("#addressLine2Billing").val($("#addressLine2Delivery").val());
    $("#addressLine3Billing").val($("#addressLine3Delivery").val()); //city
    $("#cityBilling").val($("#cityDelivery").val());
    $("#postCodeBilling").val($("#postCodeDelivery").val());
  } else {
    $("#addressLine1Billing").val("");
    $("#addressLine2Billing").val("");
    $("#addressLine3Billing").val("");
    $("#cityBilling").val("");
    $("#postCodeBilling").val("");
  }
}

function populateAddressDetailsForm(data) {
  $("#addressLine1Delivery").val(data.addressLine1Delivery);
  $("#addressLine2Delivery").val(data.addressLine2Delivery);
  $("#addressLine3Delivery").val(data.addressLine3Delivery);
  $("#cityDelivery").val(data.cityDelivery);
  $("#postCodeDelivery").val(data.postCodeDelivery);

  $("#addressLine1Billing").val(data.addressLine1Billing);
  $("#addressLine2Billing").val(data.addressLine2Billing);
  $("#addressLine3Billing").val(data.addressLine3Billing);
  $("#cityBilling").val(data.cityBilling);
  $("#postCodeBilling").val(data.postCodeBilling);
}

function buildDetailStage6ValidationRules() {
  var rules = []; // this stores all the validation rules
  rules.push("required,firstName,Please enter your first name.");
  rules.push("required,phoneNumber,Please enter your phone number.");
  rules.push("digits_only,phoneNumber,Phone number must contain only digits.");
  rules.push("required,emailAddress,Please enter your email address.");
  rules.push("valid_email,emailAddress,Please enter a valid email address.");
  return rules;
}

function buildDetailStage1ValidationRules() {
  var rules = [];

  rules.push("function,validateGarmentSelected");

  rules.push("function,validateQuantitySelected");

  rules.push("function,vaidateQualitySelected");

  rules.push(
    "digits_only,sizeSmall,Please enter a number of items that are required in size small"
  );
  rules.push(
    "digits_only,sizeMedium,Please enter a number of items that are required in size medium"
  );
  rules.push(
    "digits_only,sizeLarge,Please enter a number of items that are required in size large"
  );
  rules.push(
    "digits_only,sizeXLarge,Please enter a number of items that are required in size x-large"
  );
  rules.push(
    "digits_only,size2XLarge,Please enter a number of items that are required in size xx-large"
  );

  rules.push(
    "range>=0,sizeSmall,Please enter a number greater than or equal to 0 for size Small"
  );
  rules.push(
    "range>=0,sizeMedium,Please enter a number greater than or equal to 0 for size Medium"
  );
  rules.push(
    "range>=0,sizeLarge,Please enter a number greater than or equal to 0 for size Large"
  );
  rules.push(
    "range>=0,sizeXLarge,Please enter a number greater than or equal to 0 for size XLarge"
  );
  rules.push(
    "range>=0,size2XLarge,Please enter a number greater than or equal to 0 for size 2XLarge"
  );

  // //greater_than_or_equal
  rules.push("function,detailStage1ValidateQuantity");
  return rules;
}

function buildDetailStage4ValidationRules() {
  var rules = [];

  rules.push("required,colourFront,Please select a colour for the front");
  rules.push("required,colourBack,Please select a colour for the back");
  rules.push(
    "required,colourSleeveSelect,Please select a colour for the sleeve"
  );
  rules.push("function, detailStage4ValidateSelectedColours");
  return rules;
}

function buildHeaderStage2ValidationRules() {
  var rules = [];

  rules.push(
    "required,addressLine1Delivery,Please enter the first line of your address details for delivery"
  );
  rules.push(
    "required,cityDelivery,Please enter the city details for delivery"
  );
  rules.push(
    "required,postCodeDelivery,Please enter the post code details for delivery"
  );

  rules.push(
    "required,addressLine1Billing,Please enter the first line of your address details for billing"
  );
  rules.push("required,cityBilling,Please enter the city details for billing");
  rules.push(
    "required,postCodeBilling,Please enter the post code details for billing"
  );

  return rules;
}

function validateGarmentSelected() {
  var garmentType = $("#garmentTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");

  if (garmentType) {
    return true;
  } else {
    return [
      [
        $("#quantityTypeSelector"),
        "Please select the type of garment that you require"
      ]
    ];
  }
}

function vaidateQualitySelected() {
  var quality = $("#qualityTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");

  if (quality) {
    return true;
  } else {
    return [
      [
        $("#quantityTypeSelector"),
        "Please select the type of quality for your garments"
      ]
    ];
  }
}

function validateQuantitySelected() {
  var quantity = getSelectedQuantity();

  if (quantity > 0) {
    return true;
  } else {
    return [
      [$("#quantityTypeSelector"), "Please select a quantity that you require"]
    ];
  }
}

function detailStage1ValidateQuantity() {
  var small = parseIntSafeText("#sizeSmall");
  var medium = parseIntSafeText("#sizeMedium");
  var large = parseIntSafeText("#sizeLarge");
  var xl = parseIntSafeText("#sizeXLarge");
  var xxl = parseIntSafeText("#size2XLarge");
  var selectedQuanity = getSelectedQuantity();
  var enteredTotal = small + medium + large + xl + xxl;

  var upperSelectedQuantity = 1;
  var lowerSelectedQuantity = 1;

  if(selectedQuanity === 25)
  {
    upperSelectedQuantity = 34;
    lowerSelectedQuantity = 25;
  }
  else if (selectedQuanity === 35)
  {
    upperSelectedQuantity = 49;
    lowerSelectedQuantity = 35;
  }
  else if (selectedQuanity === 50)
  {
    upperSelectedQuantity = 74;
    lowerSelectedQuantity = 50;
  }
  else if(selectedQuanity === 75)
  {
    upperSelectedQuantity = 99;
    lowerSelectedQuantity = 75
  }
  else if(selectedQuanity === 100)
  {
    upperSelectedQuantity = 149;
    lowerSelectedQuantity = 100;
  }
  else if (selectedQuanity === 150)
  {
    upperSelectedQuantity = 199;
    lowerSelectedQuantity = 150;
  }
  else if(selectedQuanity === 200)
  {
    upperSelectedQuantity = 249;
    lowerSelectedQuantity = 200;
  }
  else 
  {
    upperSelectedQuantity = 10000;
    lowerSelectedQuantity = 250;
  }

  var isInRange = (enteredTotal >= lowerSelectedQuantity && enteredTotal <= upperSelectedQuantity);


  //if (selectedQuanity === enteredTotal && enteredTotal > 0) {
  if(isInRange && enteredTotal > 0) {
    return true;
  } else {
    return [
      [
        $("#quantity"),
        "The quantity of all sizes should equal the selected quantity amount"
      ]
    ];
  }
}

function detailStage4ValidateSelectedColours() {
  var frontNumber = parseIntSafeText("#colourFront");
  var backNumber = parseIntSafeText("#colourBack");
  var sleeveNumber = parseIntSafeText("#colourSleeveSelectDropDown");

  var total = frontNumber + backNumber + sleeveNumber;
  if (total > 0) {
    return true;
  } else {
    //check if any are 'fullColour' (and therefore has no value)
    var status =
      $("#colourFront").val() === "fullColour" ||
      $("#colourBack").val() === "fullColour" ||
      $("#colourSleeveSelectDropDown").val() === "fullColour";

    if (status) {
      return true;
    }

    return [[$("#colourTotal"), "At least one colour should be selected"]];
  }
}

function parseIntSafeText(fieldId) {
  var textVal = $(fieldId).val();
  if (textVal) {
    return parseInt(textVal);
  }
  return 0;
}

function parseIntSafeRadio(fieldName) {
  var data = $("input[type='radio'][name='" + fieldName + "']:checked").val();
  if (data) {
    return parseInt(data);
  }
  return 0;
}

function populateAddressDetails() {
  //should we use local data or the service lookup? Probably local data?
  // if (!HAVE_BOUND_ADDRESS) {
  //   $.get(CUSTOMER_DATA_URL, function(data) {
  //     if (data.found) {
  //       populateAddressDetailsForm(data);
  //     }
  //     //only do this once, otherwise will break the edit address functionality later in the flow
  //     HAVE_BOUND_ADDRESS = true;
  //   });
  // }
}

/*
    EVENT HANDLER BINDING
*/
function bindStage2Transitions() {
  //TODO - pretty sure that this can be removed...
  $('input[name="garmentType"]:radio').change(function() {
    //TODO - set the garment type text box value in here somehow....
    var dataLabel = $('input[name="garmentType"]:checked').attr("data-value");
    document.getElementById("garmentTypeText").value = dataLabel;
  });
}

function showAllRelevantStage2Controls() {
  $("#colourArea").show();
  $("#quantityArea").show();
  $("#sizesArea").show();
  $("#stage2Submit").show();
}
function handleColourPricing() {
  //this is the price from screen 2
  var currentPriceSoFar = calculateTotalPrice();
  //var quantity = getSelectedQuantity();
  var quantity = getSelectedQuantityAllItems();

  //now we need to add to this the colour weightings
  var frontColoursSelected = $("#colourFront").val();
  var backColoursSelected = $("#colourBack").val();
  var sleeveColoursSelected = $("#colourSleeveSelectDropDown").val();
  var frontPrice = calculateColourPricingImpact(quantity, frontColoursSelected);
  var backPrice = calculateColourPricingImpact(quantity, backColoursSelected);
  var sleevePrice = calculateColourPricingImpact(
    quantity,
    sleeveColoursSelected
  );

  var totalAdditionalPremiumPerGarment = frontPrice + backPrice + sleevePrice;
  var totalAdditionalPremium = totalAdditionalPremiumPerGarment * quantity;
  //need to add the additional premium to the running total.
  return totalAdditionalPremium;
}

function calculateColourPricingImpact(quantitySelected, coloursSelected) {
  //need to get the quanity from the previous form
  var quantitySelected = getSelectedQuantity();
  var runningTotal = 0;
  if (quantitySelected === 25) {
    if (coloursSelected === "1") {
      runningTotal = 350;
    } else if (coloursSelected === "2") {
      //end 1
      runningTotal = 500;
    } else if (coloursSelected === "3") {
      //end 2
      runningTotal = 650;
    } else if (coloursSelected === "4") {
      //end 3
      runningTotal = 850;
    } else if (coloursSelected === "fullColour") {
      //end 4
      runningTotal = 1050;
    } //end full colour
  } else if (quantitySelected === 35) {
    //end 25
    if (coloursSelected === "1") {
      runningTotal = 300;
    } else if (coloursSelected === "2") {
      //end 1
      runningTotal = 450;
    } else if (coloursSelected === "3") {
      //end 2
      runningTotal = 550;
    } else if (coloursSelected === "4") {
      //end 3
      runningTotal = 750;
    } else if (coloursSelected === "fullColour") {
      //end 4
      runningTotal = 850;
    } //end full colour
  } else if (quantitySelected === 50) {
    //end 35
    if (coloursSelected === "1") {
      runningTotal = 150;
    } else if (coloursSelected === "2") {
      //end 1
      runningTotal = 250;
    } else if (coloursSelected === "3") {
      //end 2
      runningTotal = 350;
    } else if (coloursSelected === "4") {
      //end 3
      runningTotal = 650;
    } else if (coloursSelected === "fullColour") {
      //end 4
      runningTotal = 700;
    } //end full colour
  } else if (quantitySelected === 75) {
    //end 50
    if (coloursSelected === "1") {
      runningTotal = 120;
    } else if (coloursSelected === "2") {
      //end 1
      runningTotal = 220;
    } else if (coloursSelected === "3") {
      //end 2
      runningTotal = 320;
    } else if (coloursSelected === "4") {
      //end 3
      runningTotal = 420;
    } else if (coloursSelected === "fullColour") {
      //end 4
      runningTotal = 450;
    } //end full colour
  } else if (quantitySelected === 100) {
    //end 75
    if (coloursSelected === "1") {
      runningTotal = 100;
    } else if (coloursSelected === "2") {
      //end 1
      runningTotal = 200;
    } else if (coloursSelected === "3") {
      //end 2
      runningTotal = 300;
    } else if (coloursSelected === "4") {
      //end 3
      runningTotal = 400;
    } else if (coloursSelected === "fullColour") {
      //end 4
      runningTotal = 400;
    } //end full colour
  } else if (quantitySelected === 150) {
    //end 100
    if (coloursSelected === "1") {
      runningTotal = 80;
    } else if (coloursSelected === "2") {
      //end 1
      runningTotal = 180;
    } else if (coloursSelected === "3") {
      //end 2
      runningTotal = 280;
    } else if (coloursSelected === "4") {
      //end 3
      runningTotal = 380;
    } else if (coloursSelected === "fullColour") {
      //end 4
      runningTotal = 390;
    } //end full colour
  } else if (quantitySelected === 200) {
    //end 150
    if (coloursSelected === "1") {
      runningTotal = 70;
    } else if (coloursSelected === "2") {
      //end 1
      runningTotal = 170;
    } else if (coloursSelected === "3") {
      //end 2
      runningTotal = 270;
    } else if (coloursSelected === "4") {
      //end 3
      runningTotal = 370;
    } else if (coloursSelected === "fullColour") {
      //end 4
      runningTotal = 380;
    } //end full colour
  } else if (quantitySelected === 250) {
    //end 200
    if (coloursSelected === "1") {
      runningTotal = 65;
    } else if (coloursSelected === "2") {
      //end 1
      runningTotal = 165;
    } else if (coloursSelected === "3") {
      //end 2
      runningTotal = 265;
    } else if (coloursSelected === "4") {
      //end 3
      runningTotal = 365;
    } else if (coloursSelected === "fullColour") {
      //end 4
      runningTotal = 370;
    } //end full colour
  } //end 250
  return runningTotal;
}

function colourFrontSelectShowHide() {
  //show them all by default, and we'll hide them later if we need to
  $("#cfItem1").show();
  $("#cfItem2").show();
  $("#cfItem3").show();
  $("#cfItem4").show();
  $("#colourFrontSelect").show();
  var colourFrontSelectedValue = $("#colourFront").val();
  if (colourFrontSelectedValue == "fullColour") {
    $("#colourFrontSelect").hide();
  } else if (colourFrontSelectedValue === "0") {
    $("#cfItem1").hide();
    $("#cfItem2").hide();
    $("#cfItem3").hide();
    $("#cfItem4").hide();
  } else if (colourFrontSelectedValue === "1") {
    $("#cfItem2").hide();
    $("#cfItem3").hide();
    $("#cfItem4").hide();
  } else if (colourFrontSelectedValue === "2") {
    $("#cfItem3").hide();
    $("#cfItem4").hide();
  } else if (colourFrontSelectedValue === "3") {
    $("#cfItem4").hide();
  } else {
    $("#colourFrontSelect").show();
  }
  totalUpPricesWithColour();
}

function colourBackSelectShowHide() {
  //show them all by default, and we'll hide them later if we need to
  $("#bfItem1").show();
  $("#bfItem2").show();
  $("#bfItem3").show();
  $("#bfItem4").show();
  $("#colourBackSelect").show();
  var colourSelectedValue = $("#colourBack").val();
  if (colourSelectedValue == "fullColour") {
    $("#colourBackSelect").hide();
  } else if (colourSelectedValue === "0") {
    $("#bfItem1").hide();
    $("#bfItem2").hide();
    $("#bfItem3").hide();
    $("#bfItem4").hide();
  } else if (colourSelectedValue === "1") {
    $("#bfItem2").hide();
    $("#bfItem3").hide();
    $("#bfItem4").hide();
  } else if (colourSelectedValue === "2") {
    $("#bfItem3").hide();
    $("#bfItem4").hide();
  } else if (colourSelectedValue === "3") {
    $("#bfItem4").hide();
  } else {
    $("#colourBackSelect").show();
  }
  totalUpPricesWithColour();
}

function colourSleeveSelectShowHide() {
  //show them all by default, and we'll hide them later if we need to
  $("#sfItem1").show();
  $("#sfItem2").show();
  $("#sfItem3").show();
  $("#sfItem4").show();
  $("#colourSleeveSelect").show();
  var colourSelectedValue = $("#colourSleeveSelectDropDown").val();
  if (colourSelectedValue == "fullColour") {
    $("#colourSleeveSelect").hide();
  } else if (colourSelectedValue === "0") {
    $("#sfItem1").hide();
    $("#sfItem2").hide();
    $("#sfItem3").hide();
    $("#sfItem4").hide();
  } else if (colourSelectedValue === "1") {
    $("#sfItem2").hide();
    $("#sfItem3").hide();
    $("#sfItem4").hide();
  } else if (colourSelectedValue === "2") {
    $("#sfItem3").hide();
    $("#sfItem4").hide();
  } else if (colourSelectedValue === "3") {
    $("#sfItem4").hide();
  } else {
    $("#colourSleeveSelect").show();
  }
  totalUpPricesWithColour();
}

function bindStage4Transitions() {
  $("#colourFront").change(colourFrontSelectShowHide);
  $("#colourBack").change(colourBackSelectShowHide);
  $("#colourSleeveSelectDropDown").change(colourSleeveSelectShowHide);
}

function bindFrontFileUploadHandlers() {
    $("#fileUploadFrontPart1").uploadFile(handleFileUploadFront1);
  $("#fileUploadFrontPart2").uploadFile(handleFileUploadFront2);
  $("#fileUploadFrontPart3").uploadFile(handleFileUploadFront3);
  $("#fileUploadFrontPart4").uploadFile(handleFileUploadFront4);
  $("#fileUploadFrontPart5").uploadFile(handleFileUploadFront5);
  $("#fileUploadFrontPart6").uploadFile(handleFileUploadFront6);
  $("#fileUploadFrontPart7").uploadFile(handleFileUploadFront7);
  $("#fileUploadFrontPart8").uploadFile(handleFileUploadFront8);
  $("#fileUploadFrontPart9").uploadFile(handleFileUploadFront9);
}

function bindFrontFileUploadTextStandard() {
    $('#fileUploadFrontPart1 .upload-text').text( 'Right Chest click to Upload');
    $('#fileUploadFrontPart2 .upload-text').text('Left Chest click to Upload');
    $('#fileUploadFrontPart3 .upload-text').text('Small Front click to Upload');
    $('#fileUploadFrontPart4 .upload-text').text('Medium Front click to Upload');
    $('#fileUploadFrontPart5 .upload-text').text('Large Front click to Upload');
    $('#fileUploadFrontPart6 .upload-text').text('Right Sleeve click to Upload');
    $('#fileUploadFrontPart7 .upload-text').text('Left Sleeve click to Upload');
    $('#fileUploadFrontPart8 .upload-text').text('Full Right Sleeve click to Upload');
    $('#fileUploadFrontPart9 .upload-text').text('Full Left Sleeve click to Upload');
}

function bindFrontFileUploadTextHoodie() {
    $('#fileUploadFrontPart1 .upload-text').text('Right Chest click to Upload');
    $('#fileUploadFrontPart2 .upload-text').text('Left Chest click to Upload');
    $('#fileUploadFrontPart3 .upload-text').text('Small Front click to Upload');
    $('#fileUploadFrontPart4 .upload-text').text('Medium Front click to Upload');
    $('#fileUploadFrontPart5 .upload-text').text('Right Sleeve click to Upload');
    $('#fileUploadFrontPart6 .upload-text').text('Left Sleeve click to Upload');
    $('#fileUploadFrontPart7 .upload-text').text('Full Right sleeve click to Upload');
    $('#fileUploadFrontPart8 .upload-text').text('Full Left Sleeve click to Upload');
    $('#fileUploadFrontPart9 .upload-text').text('d');
}


function bindFrontFileUploadTextZipped() {
    $('#fileUploadFrontPart1 .upload-text').text('Right Chest click to Upload');
    $('#fileUploadFrontPart2 .upload-text').text('Left Chest click to Upload');
    $('#fileUploadFrontPart3 .upload-text').text('Right Sleeve click to Upload');
    $('#fileUploadFrontPart4 .upload-text').text('Left Sleeve click to Upload');
    $('#fileUploadFrontPart5 .upload-text').text('Full Right sleeve click to Upload');
    $('#fileUploadFrontPart6 .upload-text').text('Full Left Sleeve click to Upload');
    $('#fileUploadFrontPart7 .upload-text').text('');
    $('#fileUploadFrontPart8 .upload-text').text('');
    $('#fileUploadFrontPart9 .upload-text').text('');
}


function bindBackFileUploadHandlers() {
  $("#fileUploadBackPartA").uploadFile(handleFileUploadBackA);
  $("#fileUploadBackPartB").uploadFile(handleFileUploadBackB);
  $("#fileUploadBackPartC").uploadFile(handleFileUploadBackC);
}

function bindBackFileUploadText() {
  $('#fileUploadBackPartA .upload-text').text('Small Back click to Upload');
  $('#fileUploadBackPartB .upload-text').text('Medium Back click to Upload');
  $('#fileUploadBackPartC .upload-text').text('Large Back Front click to Upload');
}
  

function bindFileUploadHandlers() {}

function selectCheckboxLinkItem(outerDiv, selectedItem) {
  $(outerDiv).find("*").removeClass("select_box--checked");
  $(selectedItem).addClass("select_box--checked");
}

function totalUpPrices() {
  var totalPrice = calculateTotalPrice();
  var formattedPrice = (totalPrice / 100).toFixed(2);
  storeRunningPriceTotal(formattedPrice);
}

function totalUpPricesWithColour() {
  var formattedPrice = totalUpDetailPrice();
  storeRunningPriceTotal(formattedPrice);
}

function totalUpDetailPrice() {
  var totalPrice = calculateTotalPrice();
  var additionalColourPrice = handleColourPricing();
  var formattedPrice = ((totalPrice + additionalColourPrice) / 100).toFixed(2);
  return formattedPrice;
}

function storeRunningPriceTotal(currentDetailPrice) {
  var runningTotal = 0;
  for (var i = 1; i <= getTotalNumberOfOrderDetails(); i++) {
    if (i !== getCurrentDetailNumber()) {
      var key = "detailStage4_form_price" + "_" + i;
      var price = parseFloat(localStorage[key]);
      runningTotal = runningTotal + price;
    }
  }

  var totalPrice = parseFloat(currentDetailPrice) + parseFloat(runningTotal);

  $("#totalPrice").text(totalPrice.toFixed(2));
}

function getSelectedQuantity() {
  var quantityStr = $("#quantityTypeSelector")
    .find(".select_box--checked")
    .attr("value");
  var quantity = parseInt(quantityStr) || 0;
  return quantity;
}

function getSelectedQuantityAllItems()
{
    var small = parseInt($("#sizeSmall").val()) || 0;
  var medium = parseInt($("#sizeMedium").val()) || 0;
  var large = parseInt($("#sizeLarge").val()) || 0;
  var xl = parseInt($("#sizeXLarge").val()) || 0;
  var xxl = parseInt($("#size2XLarge").val()) || 0;
  var total = small + medium + large + xl + xxl;

  return total;
}

function calculateTotalPrice() {
  var runningTotal = 0;
  //get the selected gartment type and the quality that we would like
  var garmentType = $("#garmentTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");
  var quality = $("#qualityTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");
 // var quantity = getSelectedQuantity();
var quantity = getSelectedQuantityAllItems();
  if (quantity > 50) {
    if (garmentType === "Unisex T-Shirt") {
      if (quality === "Basic") {
        runningTotal = 220;
      } else if (quality === "Standard") {
        runningTotal = 240;
      } else if (quality === "Premium") {
        runningTotal = 280;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Ladies T-Shirt") {
      //end unisex
      if (quality === "Basic") {
        runningTotal = 240;
      } else if (quality === "Standard") {
        runningTotal = 260;
      } else if (quality === "Premium") {
        runningTotal = 290;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Kids T-Shirt") {
      //end ladies t shirt
      if (quality === "Basic") {
        runningTotal = 180;
      } else if (quality === "Standard") {
        runningTotal = 200;
      } else if (quality === "Premium") {
        runningTotal = 360;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Long Sleeve T-Shirt") {
      //end kids
      if (quality === "Basic") {
        runningTotal = 370;
      } else if (quality === "Standard") {
        runningTotal = 430;
      } else if (quality === "Premium") {
        runningTotal = 650;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Sweat Shirt") {
      //end long sleeve
      if (quality === "Basic") {
        runningTotal = 720;
      } else if (quality === "Standard") {
        runningTotal = 830;
      } else if (quality === "Premium") {
        runningTotal = 940;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Hooded Top") {
      //end sweat shirt
      if (quality === "Basic") {
        runningTotal = 990;
      } else if (quality === "Standard") {
        runningTotal = 1100;
      } else if (quality === "Premium") {
        runningTotal = 1530;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Zipped Hoodies") {
      //end hooded top
      if (quality === "Basic") {
        runningTotal = 1250;
      } else if (quality === "Standard") {
        runningTotal = 1400;
      } else if (quality === "Premium") {
        runningTotal = 1900;
      } else {
        runningTotal = 0;
      }
    } //end zipped hoodies
  } else {
    //end quantity greater than 50
    if (garmentType === "Unisex T-Shirt") {
      if (quality === "Basic") {
        runningTotal = 250;
      } else if (quality === "Standard") {
        runningTotal = 280;
      } else if (quality === "Premium") {
        runningTotal = 320;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Ladies T-Shirt") {
      //end unisex
      if (quality === "Basic") {
        runningTotal = 280;
      } else if (quality === "Standard") {
        runningTotal = 300;
      } else if (quality === "Premium") {
        runningTotal = 330;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Kids T-Shirt") {
      //end ladies t shirt
      if (quality === "Basic") {
        runningTotal = 200;
      } else if (quality === "Standard") {
        runningTotal = 230;
      } else if (quality === "Premium") {
        runningTotal = 410;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Long Sleeve T-Shirt") {
      //end kids
      if (quality === "Basic") {
        runningTotal = 420;
      } else if (quality === "Standard") {
        runningTotal = 490;
      } else if (quality === "Premium") {
        runningTotal = 750;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Sweat Shirt") {
      //end long sleeve
      if (quality === "Basic") {
        runningTotal = 820;
      } else if (quality === "Standard") {
        runningTotal = 950;
      } else if (quality === "Premium") {
        runningTotal = 1050;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Hooded Top") {
      //end sweat shirt
      if (quality === "Basic") {
        runningTotal = 1100;
      } else if (quality === "Standard") {
        runningTotal = 1350;
      } else if (quality === "Premium") {
        runningTotal = 1750;
      } else {
        runningTotal = 0;
      }
    } else if (garmentType === "Zipped Hoodies") {
      //end hooded top
      if (quality === "Basic") {
        runningTotal = 1400;
      } else if (quality === "Standard") {
        runningTotal = 1600;
      } else if (quality === "Premium") {
        runningTotal = 2100;
      } else {
        runningTotal = 0;
      }
    } //end zipped hoodies
  } //end less than 75
  if (quantity > 0) {
    return runningTotal * quantity;
  } else {
    return runningTotal;
  }
} //end method

function updateColourPickerOptionsBasedOnSelection() {
  var garmentType = $("#garmentTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");
  var quality = $("#qualityTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");
  if (garmentType && quality) {
    $("#colourArea").addClass("js-show");
    $(".container__color-pick").removeClass("js-show");
    if (garmentType === "Unisex T-Shirt") {
      if (quality === "Basic") {
        $(".garmentcolor-uni-b").addClass("js-show");
      } else if (quality === "Standard") {
        $(".garmentcolor-uni-s").addClass("js-show");
      } else if (quality === "Premium") {
        $(".garmentcolor-uni-p").addClass("js-show");
      }
    } else if (garmentType === "Ladies T-Shirt") {
      //end unisex
      if (quality === "Basic") {
        $(".garmentcolor-ladies-b").addClass("js-show");
      } else if (quality === "Standard") {
        $(".garmentcolor-ladies-s").addClass("js-show");
      } else if (quality === "Premium") {
        $(".garmentcolor-ladies-p").addClass("js-show");
      }
    } else if (garmentType === "Kids T-Shirt") {
      //end ladies t shirt
      if (quality === "Basic") {
        $(".garmentcolor-kids-b").addClass("js-show");
      } else if (quality === "Standard") {
        $(".garmentcolor-kids-s").addClass("js-show");
      } else if (quality === "Premium") {
        $(".garmentcolor-kids-p").addClass("js-show");
      }
    } else if (garmentType === "Long Sleeve T-Shirt") {
      //end kids
      if (quality === "Basic") {
        $(".garmentcolor-longsleeve-b").addClass("js-show");
      } else if (quality === "Standard") {
        $(".garmentcolor-longsleeve-s").addClass("js-show");
      } else if (quality === "Premium") {
        $(".garmentcolor-longsleeve-p").addClass("js-show");
      }
    } else if (garmentType === "Sweat Shirt") {
      //end long sleeve
      if (quality === "Basic") {
        $(".garmentcolor-sweat-b").addClass("js-show");
      } else if (quality === "Standard") {
        $(".garmentcolor-sweat-s").addClass("js-show");
      } else if (quality === "Premium") {
        $(".garmentcolor-sweat-p").addClass("js-show");
      }
    } else if (garmentType === "Hooded Top") {
      //end sweat shirt
      if (quality === "Basic") {
        $(".garmentcolor-hoodedtop-b").addClass("js-show");
      } else if (quality === "Standard") {
        $(".garmentcolor-hoodedtop-s").addClass("js-show");
      } else if (quality === "Premium") {
        $(".garmentcolor-hoodedtop-p").addClass("js-show");
      }
    } else if (garmentType === "Zipped Hoodies") {
      //end hooded top
      if (quality === "Basic") {
        $(".garmentcolor-zip-b").addClass("js-show");
      } else if (quality === "Standard") {
        $(".garmentcolor-zip-s").addClass("js-show");
      } else if (quality === "Premium") {
        $(".garmentcolor-zip-p").addClass("js-show");
      }
    } //end zipped hoodies
  } //end if Garment selected
}

function updateCareDetailsLink() {
  var garmentType = $("#garmentTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");
  var quality = $("#qualityTypeSelector")
    .find(".select_box--checked")
    .attr("data-value");
  if (garmentType && quality) {
    var linkText = quality + " " + garmentType;
    var linkUrl = "";
    if (garmentType === "Unisex T-Shirt") {
      if (quality === "Basic") {
        linkUrl = "/unisex-t-shirt-basic";
      } else if (quality === "Standard") {
        linkUrl = "/unisex-t-shirt-standard";
      } else if (quality === "Premium") {
        linkUrl = "/unisex-t-shirt-premium";
      }
    } else if (garmentType === "Ladies T-Shirt") {
      //end unisex
      if (quality === "Basic") {
        linkUrl = "/ladies-t-shirt-basic";
      } else if (quality === "Standard") {
        linkUrl = "/ladies-t-shirt-standard";
      } else if (quality === "Premium") {
        linkUrl = "/ladies-t-shirt-premium";
      }
    } else if (garmentType === "Kids T-Shirt") {
      //end ladies t shirt
      if (quality === "Basic") {
        linkUrl = "/kids-t-shirt-basic";
      } else if (quality === "Standard") {
        linkUrl = "/kids-t-shirt-standard";
      } else if (quality === "Premium") {
        linkUrl = "/kids-t-shirt-premium";
      }
    } else if (garmentType === "Long Sleeve T-Shirt") {
      //end kids
      if (quality === "Basic") {
        linkUrl = "/long-sleeve-t-shirt-basic";
      } else if (quality === "Standard") {
        linkUrl = "/long-sleeve-t-shirt-standard";
      } else if (quality === "Premium") {
        linkUrl = "/long-sleeve-t-shirt-premium";
      }
    } else if (garmentType === "Sweat Shirt") {
      //end long sleeve
      if (quality === "Basic") {
        linkUrl = "/sweat-shirt-basic";
      } else if (quality === "Standard") {
        linkUrl = "/sweat-shirt-standard";
      } else if (quality === "Premium") {
        linkUrl = "/sweat-shirt-premium";
      }
    } else if (garmentType === "Hooded Top") {
      //end sweat shirt
      if (quality === "Basic") {
        linkUrl = "/hooded-top-basic";
      } else if (quality === "Standard") {
        linkUrl = "/hooded-top-standard";
      } else if (quality === "Premium") {
        linkUrl = "/hooded-top-premium";
      }
    } else if (garmentType === "Zipped Hoodies") {
      //end hooded top
      if (quality === "Basic") {
        linkUrl = "/zipped-hoodies-basic";
      } else if (quality === "Standard") {
        linkUrl = "/zipped-hoodies-standard";
      } else if (quality === "Premium") {
        linkUrl = "/zipped-hoodies-premium";
      }
    } //end zipped hoodies
    //now we need to update the Link with the relevant information...
    $("#aftercareLinkUrl").text(linkText);
    $("#aftercareLinkUrl").prop("href", linkUrl);
    $("#infoArea").show();
  } else {
    //end if Garment selected
    $("#infoArea").hide();
  }
} //end method

function totalUpSizes() {
  var small = parseInt($("#sizeSmall").val()) || 0;
  var medium = parseInt($("#sizeMedium").val()) || 0;
  var large = parseInt($("#sizeLarge").val()) || 0;
  var xl = parseInt($("#sizeXLarge").val()) || 0;
  var xxl = parseInt($("#size2XLarge").val()) || 0;
  var total = small + medium + large + xl + xxl;
  var selectedQuantInt = getSelectedQuantity();

  var upperSelectedQuantity = 1;
  var lowerSelectedQuantity = 1;

  if(selectedQuantInt === 25)
  {
    upperSelectedQuantity = 34;
    lowerSelectedQuantity = 25;
  }
  else if (selectedQuantInt === 35)
  {
    upperSelectedQuantity = 49;
    lowerSelectedQuantity = 35;
  }
  else if (selectedQuantInt === 50)
  {
    upperSelectedQuantity = 74;
    lowerSelectedQuantity = 50;
  }
  else if(selectedQuantInt === 75)
  {
    upperSelectedQuantity = 99;
    lowerSelectedQuantity = 75
  }
  else if(selectedQuantInt === 100)
  {
    upperSelectedQuantity = 149;
    lowerSelectedQuantity = 100;
  }
  else if (selectedQuantInt === 150)
  {
    upperSelectedQuantity = 199;
    lowerSelectedQuantity = 150;
  }
  else if(selectedQuantInt === 200)
  {
    upperSelectedQuantity = 249;
    lowerSelectedQuantity = 200;
  }
  else 
  {
    upperSelectedQuantity = 10000;
    lowerSelectedQuantity = 250;
  }

  var isInRange = (total >= lowerSelectedQuantity && total <= upperSelectedQuantity);

  $("#sizes-validation").text(" = " + total);
  //if (selectedQuantInt > 0 && total === selectedQuantInt) {
  if(selectedQuantInt > 0 && isInRange ) {
    $("#sizes-validation").addClass("sizes-validation--tick");
  } else {
    $("#sizes-validation").removeClass("sizes-validation--tick");
  }
}

function bindDetailStage1Handlers() {
  $("#gtUnisex").click(function() {
    selectCheckboxLinkItem("#garmentTypeSelector", "#gtUnisex");
    totalUpPrices();
    updateCareDetailsLink();
    updateColourPickerOptionsBasedOnSelection();
  });
  $("#gtLadiesT").click(function() {
    selectCheckboxLinkItem("#garmentTypeSelector", "#gtLadiesT");
    totalUpPrices();
    updateCareDetailsLink();
    updateColourPickerOptionsBasedOnSelection();
  });
  $("#gtKidsT").click(function() {
    selectCheckboxLinkItem("#garmentTypeSelector", "#gtKidsT");
    totalUpPrices();
    updateCareDetailsLink();
    updateColourPickerOptionsBasedOnSelection();
  });
  $("#gtLongSleevedT").click(function() {
    selectCheckboxLinkItem("#garmentTypeSelector", "#gtLongSleevedT");
    totalUpPrices();
    updateCareDetailsLink();
    updateColourPickerOptionsBasedOnSelection();
  });
  $("#gtSweatShirt").click(function() {
    selectCheckboxLinkItem("#garmentTypeSelector", "#gtSweatShirt");
    totalUpPrices();
    updateCareDetailsLink();
    updateColourPickerOptionsBasedOnSelection();
  });
  $("#gtHoodedTop").click(function() {
    selectCheckboxLinkItem("#garmentTypeSelector", "#gtHoodedTop");

    totalUpPrices();
    updateCareDetailsLink();
    updateColourPickerOptionsBasedOnSelection();
  });
  $("#gtZippedHood").click(function() {
    selectCheckboxLinkItem("#garmentTypeSelector", "#gtZippedHood");
    totalUpPrices();
    updateCareDetailsLink();
    updateColourPickerOptionsBasedOnSelection();
  });

  $("#qualBasic").click(function() {
    selectCheckboxLinkItem("#qualityTypeSelector", "#qualBasic");
    totalUpPrices();
    updateCareDetailsLink();
    updateColourPickerOptionsBasedOnSelection();
  });
  $("#qualStandard").click(function() {
    selectCheckboxLinkItem("#qualityTypeSelector", "#qualStandard");
    totalUpPrices();
    updateCareDetailsLink();
    updateColourPickerOptionsBasedOnSelection();
  });
  $("#qualPremium").click(function() {
    selectCheckboxLinkItem("#qualityTypeSelector", "#qualPremium");
    totalUpPrices();
    updateCareDetailsLink();
    updateColourPickerOptionsBasedOnSelection();
  });

  $("#quan25").click(function() {
    selectCheckboxLinkItem("#quantityTypeSelector", "#quan25");
    totalUpSizes();
    totalUpPrices();
  });
  $("#quan35").click(function() {
    selectCheckboxLinkItem("#quantityTypeSelector", "#quan35");
    totalUpSizes();
    totalUpPrices();
  });
  $("#quan50").click(function() {
    selectCheckboxLinkItem("#quantityTypeSelector", "#quan50");
    totalUpSizes();
    totalUpPrices();
  });
  $("#quan75").click(function() {
    selectCheckboxLinkItem("#quantityTypeSelector", "#quan75");
    totalUpSizes();
    totalUpPrices();
  });
  $("#quan100").click(function() {
    selectCheckboxLinkItem("#quantityTypeSelector", "#quan100");
    totalUpSizes();
    totalUpPrices();
  });
  $("#quan150").click(function() {
    selectCheckboxLinkItem("#quantityTypeSelector", "#quan150");
    totalUpSizes();
    totalUpPrices();
  });
  $("#quan200").click(function() {
    selectCheckboxLinkItem("#quantityTypeSelector", "#quan200");
    totalUpSizes();
    totalUpPrices();
  });
  $("#quan250").click(function() {
    selectCheckboxLinkItem("#quantityTypeSelector", "#quan250");
    totalUpSizes();
    totalUpPrices();
  });

  $("#sizeSmall").blur(function() {
    totalUpSizes();
    totalUpPrices();
  });
  $("#sizeMedium").blur(function() {
    totalUpSizes();
     totalUpPrices();
  });
  $("#sizeLarge").blur(function() {
    totalUpSizes();
     totalUpPrices();
  });
  $("#sizeXLarge").blur(function() {
    totalUpSizes();
     totalUpPrices();
  });
  $("#size2XLarge").blur(function() {
    totalUpSizes();
     totalUpPrices();
  });
}

function disableEnterOnTextBoxes() {
  $("#sizeSmall").onEnter(function() {
    return false;
  });
  $("#sizeMedium").onEnter(function() {
    return false;
  });
  $("#sizeLarge").onEnter(function() {
    return false;
  });
  $("#sizeXLarge").onEnter(function() {
    return false;
  });
  $("#size2XLarge").onEnter(function() {
    return false;
  });
}

/**
 * @version 1.1
 * 
 * @since 1.1 Changed event handles to use new order.
 */
function attachCoreEventHandlers() {
  $("#clearReturningUserData").click(clearReturningUserData);
  $("#continueReturningUserData").click(continueReturningUserData);

  $("#odStage1Submit").click(odStage1Submit);
  $("#odStage1Back").click(odStage1Back);

  $("#odStage2Submit").click(odStage2Submit);
  $("#odStage2Back").click(odStage2Back);

  $("#odStage3Submit").click(odStage3Submit);
  $("#odStage3Back").click(odStage3Back);

  $("#odStage4Submit").click(odStage4Submit);
  $("#odStage4Back").click(odStage4Back);

  $("#odStage5Submit").click(odStage5Submit);
  $("#odStage5Back").click(odStage5Back);
  $("#odStage5AddGarment").click(odStage5AddGarment);

  $("#odStage6Submit").click(odStage6Submit);
  $("#odStage6Back").click(odStage6Back);
  //$("#stage1Reset").click(stage1Reset);

  $("#odStage7Submit").click(odStage7Submit);
  $("#odStage7Back").click(odStage7Back);

  $("#odStage8Back").click(odStage8Back);
  $("#odStage8Submit").click(odStage8Submit);
  $("#orderDetailsStage8EditAddressSumbit").click(
    orderHeaderStage_8EditAddressSumbit
  );
  $("#orderDetailsStage8UpdateAddressCancel").click(
    orderHeaderStage8UpdateAddressCancel
  );
  $("#orderDetailsStage8UpdateAddressSumbit").click(
    orderHeaderStage8UpdateAddressSumbit
  );

  $("#createAccount").change(passwordShowHide);
  $("#useDeliveryAddress").change(useDeliveryAddress);
  $("#createAccountSummary").change(createPasswordSummaryChanged);

  $("#reuseArtworkYes").click(addAnotherGarmentReuseFiles);

  $("#reuseArtworkNo").click(addAnotherGarmentDontReUseFiles);

  bindDetailStage1Handlers();

  bindStage2Transitions();
  bindStage4Transitions();

  bindFileUploadHandlers();

  disableEnterOnTextBoxes();
}

function hideStageComponents() {
  $(
    "#detailStage2, #detailStage3, #detailStage4, #detailStage5, #detailStage6, #detailStage7, #detailStage8"
  ).hide();

  $("#passwordFormArea").hide();
  $("#addressOrderDetailsEdit").hide();

  $("#addAnotherGarmentPanel").hide();
}

function continueReturningUserData() {
  //get hold of modal and close it
  var demo = $("#js-returnuser").animatedModal();
  demo.close();

  //$("#returningUserNotification").hide();
}

function clearReturningUserData() {
  localStorage.clear();
  clearForm("");

  //get hold of modal and close it
  var demo = $("#js-returnuser").animatedModal();
  demo.close();
  // $("#returningUserNotification").hide();
}

function checkForReturningUser() {
  var returningUser = localStorage["returningUser"];
  if (returningUser !== undefined) {
    var demo = $("#js-returnuser").animatedModal({
      animatedIn: "bounceIn",
      animatedOut: "bounceOut",
      color: "#ffcb04"
    });
    demo.open();

    // $("#returningUserNotification").show();
  } else {
    $("#animatedModal").hide();
  }
}

/**
 * @version 1.1
 *   Entry point for the whole page process
 * @since 1.1 Changed starting page to detailStage1 instead of detailStage7
 */
function bindPage() {
  checkForReturningUser();

  attachCoreEventHandlers();
  hideStageComponents();

  $("#estimatedDelivery").text(GetDeliveryDate());
  $("#detailStage1").show();
  bindDetail1Data();
  updateBodyClassStage(1);
}

$(document).ready(function() {
  // MODALS

  // COLOUR PICKERS

  // Unisex Tshirts (b: basic, s: standard & p: premium)

  // Basic
  $('[name="garmentcolor-uni-b"]').paletteColorPicker({
    colors: [
      { "Light Pink": "#FFC0F5" },
      { Heliconia: "#FF1880" },
      { Orange: "#FF7B00" },
      { "Daisy Yellow": "#FBED00" },
      { Lime: "#B5CE38" },
      { "Irish Green": "#00A44F" },
      { "Military Green": "#5B6635" },
      { "Forest Green": "#0F4823" },
      { "Light Blue": "#BBE3FF" },
      { Sapphire: "#00A7FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { "Indigo Blue": "#457DA3" },
      { Purple: "#430077" },
      { Natural: "#ECE7D2" },
      { Black: "#000000" },
      { White: "#FFFFFF" },
      { Charcoal: "#52575B" },
      { Red: "#D8273A" },
      { "Cardinal Red": "#900000" },
      { "Dark Chocolate": "#3F0303" },
      { "Sports Grey": "#C3C2BE" },
      { "Antique Cherry Red": "#902A2A" },
      { "Antique Heliconia": "#AD1E66" },
      { "Antique Sapphire": "#006DA8" },
      { "Dark Heather": "#2D2A2A" },
      { "Heather Navy": "#28335B" },
      { "Heather Military Green": "#6F9564" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-uni-b").attr("selected-value", clicked_color);
    }
  });

  // Standard
  $('[name="garmentcolor-uni-s"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Natural: "#ECE7D2" },
      { Charcoal: "#52575B" },
      { Black: "#000000" },
      { Red: "#D8273A" },
      { "Cardinal Red": "#900000" },
      { "Light Pink": "#FFC0F5" },
      { Heliconia: "#FF1880" },
      { Orange: "#FF7B00" },
      { "Daisy Yellow": "#FBED00" },
      { Lime: "#B5CE38" },
      { "Irish Green": "#00A44F" },
      { "Military Green": "#5B6635" },
      { "Forest Green": "#0F4823" },
      { "Light Blue": "#BBE3FF" },
      { Sapphire: "#00A7FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { "Indigo Blue": "#457DA3" },
      { Purple: "#430077" },
      { "Dark Chocolate": "#3F0303" },
      { "Sports Grey": "#C3C2BE" },
      { "Antique Cherry Red": "#902A2A" },
      { "Antique Heliconia": "#AD1E66" },
      { "Antique Sapphire": "#006DA8" },
      { "Dark Heather": "#2D2A2A" },
      { "Heather Navy ": "#28335B" },
      { "Heather Military Green": "#6F9564" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {
    },
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-uni-s").attr("selected-value", clicked_color);
    }
  });

  // Premium
  $('[name="garmentcolor-uni-p"]').paletteColorPicker({
    colors: [
      { Black: "#000000" },
      { White: "#FFFFFF" },
      { Charcoal: "#52575B" },
      { Red: "#D8273A" },
      { Azalea: "#FF9FF5" },
      { Heliconia: "#FF1880" },
      { Orange: "#FF7B00" },
      { "Daisy Yellow": "#FBED00" },
      { Lime: "#B5CE38" },
      { "Irish Green": "#00A44F" },
      { "Military Green": "#5B6635" },
      { "Forest Green": "#0F4823" },
      { "Light Blue": "#BBE3FF" },
      { Sapphire: "#00A7FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { Purple: "#430077" },
      { "Sports Grey ": "#C3C2BE" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {
    },
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-uni-p").attr("selected-value", clicked_color);
    }
  });

  // Ladies Tshirts (b: basic, s: standard & p: premium)

  // Basic
  $('[name="garmentcolor-ladies-b"]').paletteColorPicker({
    colors: [
      { Black: "#000000" },
      { White: "#FFFFFF" },
      { "Light Graphite": "#62665D" },
      { Red: "#D8273A" },
      { Burgandy: "#6B1840" },
      { "Light Pink": "#FFC0F5" },
      { Orange: "#FF7B00" },
      { Fushia: "#FF1880" },
      { Sunflower: "#FBD800" },
      { "Kelly Green": "#128747" },
      { "Bottle Green": "#0F4823" },
      { "Sky Blue": "#BBE3FF" },
      { "AZure Blue": "#0090FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { Purple: "#430077" },
      { "Heather Grey ": "#C3C2BE" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {
    },
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-ladies-b").attr("selected-value", clicked_color);
    }
  });

  // Standard
  $('[name="garmentcolor-ladies-s"]').paletteColorPicker({
    colors: [
      { Black: "#000000" },
      { White: "#FFFFFF" },
      { Sand: "#ECE7B3" },
      { Charcoal: "#52575B" },
      { Red: "#D8273A" },
      { "Cherry Red": "#A50000" },
      { "Light Pink": "#FFC0F5" },
      { Heliconia: "#FF1880" },
      { Orange: "#FF7B00" },
      { "Daisy Yellow": "#FBED00" },
      { Kiwi: "#B5C507" },
      { "Irish Green": "#00A44F" },
      { "Light Blue": "#BBE3FF" },
      { Sapphire: "#00A7FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { "Indigo Blue": "#457DA3" },
      { Purple: "#430077" },
      { "Dark Chocolate": "#3F0303" },
      { "Sports Grey": "#902A2A" },
      { "Antique Heliconia ": "#AD1E66" },
      { "Antique Sapphire": "#2D2A2A" },
      { "Dark Heather": "#2D2A2A" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {
    },
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-ladies-s").attr("selected-value", clicked_color);
    }
  });

  // Premium
  $('[name="garmentcolor-ladies-p"]').paletteColorPicker({
    colors: [
      { Black: "#000000" },
      { White: "#FFFFFF" },
      { Red: "#D8273A" },
      { Azalea: "#FF9FF5" },
      { Heliconia: "#FF1880" },
      { CornSilk: "#FBED92" },
      { "Light Blue ": "#BBE3FF" },
      { Sapphire: "#00A7FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { "Sports Grey": "#C3C2BE" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {
    },
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-ladies-p").attr("selected-value", clicked_color);
    }
  });

  // Sweatshirts (b: basic, s: standard & p: premium)

  // Basic
  $('[name="garmentcolor-sweat-b"]').paletteColorPicker({
    colors: [
      { Black: "#000000" },
      { White: "#FFFFFF" },
      { Sand: "#D3C89B" },
      { Charcoal: "#52575B" },
      { Red: "#D8273A" },
      { Maroon: "#6D0808" },
      { "Light Pink": "#FFC0F5" },
      { Heliconia: "#FF1880" },
      { Orange: "#FF7B00" },
      { Gold: "#FCD703" },
      { Kiwi: "#B5CE00" },
      { "Irish Green": "#00A44F" },
      { "Military Green": "#5B6635" },
      { "Forest Green": "#0F4823" },
      { "Light Blue": "#BBE3FF" },
      { Sapphire: "#00A7FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { "Indigo Blue": "#457DA3" },
      { Purple: "#430077" },
      { "Dark Chocolate": "#3F0303" },
      { "Sports Grey": "#C3C2BE" },
      { "Antique Cherry Red": "#902A2A" },
      { "Antique Sapphire": "#006DA8" },
      { "Dark Heather": "#2D2A2A" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {
    },
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-sweat-b").attr("selected-value", clicked_color);
    }
  });

  // Standard
  $('[name="garmentcolor-sweat-s"]').paletteColorPicker({
    colors: [
      { "Arctic White": "#FFFFFF" },
      { "Jet Black": "#000000" },
      { "Storm Grey ": "#52575B" },
      { "Fire Red": "#FF0000" },
      { Burgandy: "#720D2E" },
      { "Baby Pink": "#FFC0F5" },
      { "Hot Pink": "#FF1880" },
      { "Orange Crush": "#FF7B00" },
      { "Sun Yellow": "#FBED00" },
      { "Lime Green": "#B5CE38" },
      { "Kelly Green": "#00A161" },
      { "Olive Green": "#717C42" },
      { "Forest Green": "#0F4823" },
      { "Tropical Blue": "#00A7FF" },
      { Royal: "#002CB2" },
      { "Airfoce Blue": "#4A7891" },
      { Purple: "#430077" },
      { "Hot Chocolate": "#541805" },
      { "Heather Grey": "#C3C2BE" },
      { "Charcoal Dark Heather": "#2D2A2A" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {
    },
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-sweat-s").attr("selected-value", clicked_color);
    }
  });

  // Premium
  $('[name="garmentcolor-sweat-p"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Black: "#000000" },
      { Anthracite: "#52575B" },
      { Red: "#D8273A" },
      { Orange: "#FF7B00" },
      { "Chilli Gold": "#FCBB03" },
      { "Kelly Green": "#009B23" },
      { "Forest Green": "#0F4823" },
      { "Light Blue": "#BBE3FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { Purple: "#430077" },
      { "Heather Grey": "#C3C2BE" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {
    },
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-sweat-p").attr("selected-value", clicked_color);
    }
  });

  // Zip Hooded Sweat

  // Basic
  $('[name="garmentcolor-zip-b"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Black: "#000000" },
      { Red: "#D8273A" },
      { Maroon: "#6D0808" },
      { "Light Pink": "#FFC0F5" },
      { Orange: "#FF7B00" },
      { "Irish Green": "#00A44F" },
      { "Forest Green": "#0F4823" },
      { "Carolina Blue": "#92C9FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { Purple: "#430077" },
      { "Dark Chocolate": "#3F0303" },
      { "Sports Grey": "#C3C2BE" },
      { "Dark Heather": "#2D2A2A" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {
    },
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-zip-b").attr("selected-value", clicked_color);
    }
  });

  // Standard
  $('[name="garmentcolor-zip-s"]').paletteColorPicker({
    colors: [
      { "Arctic White": "#FFFFFF" },
      { "Jet Black": "#000000" },
      { "Steel Grey": "#5C5D5E" },
      { "Fire Red": "#FF0000" },
      { Burgandy: "#720D2E" },
      { "Hot Pink": "#FF1880" },
      { "Orange Crush": "#FF7B00" },
      { "Sun Yellow": "#FBED00" },
      { "Kelly Green": "#00A161" },
      { "Forest Green": "#0F4823" },
      { "Sapphire Blue": "#00A7FF" },
      { Royal: "#002CB2" },
      { "Oxford Navy": "#00065F" },
      { Purple: "#430077" },
      { "Heather Grey": "#C3C2BE" },
      { "Charcoal Dark Heather": "#2D2A2A" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {
    },
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-zip-s").attr("selected-value", clicked_color);
    }
  });

  // Premium
  $('[name="garmentcolor-zip-p"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Black: "#000000" },
      { Red: "#D8273A" },
      { Fushia: "#E52589" },
      { Lime: "#B5ED00" },
      { Apple: "#3CC66D" },
      { "Bottle Green": "#0F4823" },
      { "Sky Blue": "#BBE3FF" },
      { Purple: "#4A186B" },
      { Royal: "#002CB2" },
      { "French Navy": "#00065F" },
      { "Light Oxford": "#C3C2BE" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-zip-p").attr("selected-value", clicked_color);
    }
  });

  // Kids Tshirts

  // Basic
  $('[name="garmentcolor-kids-b"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Black: "#000000" },
      { "Light Graphite": "#62665D" },
      { Red: "#D8273A" },
      { Burgandy: "#6B1840" },
      { Fushia: "#FF1880" },
      { Orange: "#FF7B00" },
      { Yellow: "#FBED00" },
      { Lime: "#B5CE38" },
      { "Kelly Green": "#128747" },
      { "Classic Olive": "#737F41" },
      { "Bottle Green": "#0F4823" },
      { "Sky Blue": "#BBE3FF" },
      { "Azure Blue": "#0090FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { Purple: "#430077" },
      { "Heather Grey": "#C3C2BE" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-kids-b").attr("selected-value", clicked_color);
    }
  });

  // Standard
  $('[name="garmentcolor-kids-s"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Natural: "#ECE7D2" },
      { Black: "#000000" },
      { Charcoal: "#52575B" },
      { Red: "#D8273A" },
      { "Cardinal Red": "#900000" },
      { "Light Pink": "#FFC0F5" },
      { Heliconia: "#FF1880" },
      { Orange: "#FF7B00" },
      { "Daisy Yellow": "#FBED00" },
      { Lime: "#B5CE38" },
      { "Irish Green": "#00A44F" },
      { "Military Green": "#5B6635" },
      { "Forest Green": "#0F4823" },
      { "Light Blue": "#BBE3FF" },
      { Sapphire: "#00A7FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { "Indigo Blue": "#457DA3" },
      { Purple: "#430077" },
      { "Dark Chocolate": "#3F0303" },
      { "Sports Grey": "#C3C2BE" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-kids-s").attr("selected-value", clicked_color);
    }
  });

  // Premium
  $('[name="garmentcolor-kids-p"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Black: "#000000" },
      { Red: "#D8273A" },
      { Burgandy: "#6B1840" },
      { Yellow: "#FBED00" },
      { "Winter Emerald": "#0AA56D" },
      { "Bottle Green": "#0F4823" },
      { "Sky Blue": "#BBE3FF" },
      { "AZure Blue": "#0090FF" },
      { Royal: "#002CB2" },
      { "French Navy": "#00065F" },
      { "Light Oxford": "#C3C2BE" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-kids-p").attr("selected-value", clicked_color);
    }
  });

  // Long Sleeve T-shirt - garmentcolor-longsleeve-b

  // Basic
  $('[name="garmentcolor-longsleeve-b"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Black: "#000000" },
      { Red: "#D8273A" },
      { Royal: "#002CB2" },
      { "Deep Navy": "#00065F" },
      { "Heather Grey": "#C3C2BE" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-longsleeve-b").attr("selected-value", clicked_color);
    }
  });

  // Standard
  $('[name="garmentcolor-longsleeve-s"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Black: "#000000" },
      { Charcoal: "#52575B" },
      { Red: "#D8273A" },
      { Orange: "#FF7B00" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { "Sports Grey": "#C3C2BE" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-longsleeve-s").attr("selected-value", clicked_color);
    }
  });

  // Premium
  $('[name="garmentcolor-longsleeve-p"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Black: "#000000" },
      { "Deep Red": "#AD142A" },
      { Brown: "#4C2406" },
      { "Dark Grey": "#474747" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { "Light Oxford": "#C3C2BE" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-longsleeve-p").attr("selected-value", clicked_color);
    }
  });

  // Hooded Top - garmentcolor-hoodedtop-b

  // Basic
  $('[name="garmentcolor-hoodedtop-b"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Sand: "#D3C89B" },
      { Black: "#000000" },
      { Charcoal: "#52575B" },
      { Red: "#D8273A" },
      { Maroon: "#900000" },
      { "Light Pink": "#FFC0F5" },
      { Heliconia: "#FF1880" },
      { Orange: "#FF7B00" },
      { Gold: "#FCD703" },
      { Kiwi: "#B5CE00" },
      { "Irish Green": "#00A44F" },
      { "Military Green": "#5B6635" },
      { "Forest Green": "#0F4823" },
      { "Light Blue": "#BBE3FF" },
      { Sapphire: "#00A7FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { "Indigo Blue": "#457DA3" },
      { Purple: "#430077" },
      { "Dark Chocolate": "#3F0303" },
      { "Sports Grey": "#C3C2BE" },
      { "Antique Cherry Red": "#902A2A" },
      { "Antique Sapphire": "#006DA8" },
      { "Dark Heather": "#2D2A2A" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-hoodedtop-b").attr("selected-value", clicked_color);
    }
  });

  // Standard
  $('[name="garmentcolor-hoodedtop-s"]').paletteColorPicker({
    colors: [
      { "Arctic White": "#FFFFFF" },
      { "Dessert Sand": "#CCC296" },
      { "Jet Black": "#000000" },
      { "Storm Grey": "#52575B" },
      { "Fire Red": "#FF0000" },
      { Burgandy: "#720D2E" },
      { "Baby Pink": "#FFC0F5" },
      { "Hot Pink": "#FF1880" },
      { "Orange Crush": "#FF7B00" },
      { "Sun Yellow ": "#FBED00" },
      { "Lime Green": "#B5CE38" },
      { "Kelly Green": "#00A161" },
      { "Olive Green": "#717C42" },
      { "Forest Green": "#0F4823" },
      { "Sky Blue": "#BBE3FF" },
      { "Tropical Blue": "#00A7FF" },
      { Royal: "#002CB2" },
      { Navy: "#00065F" },
      { "Airfoce Blue": "#4A7891" },
      { Purple: "#430077" },
      { "Hot Chocolate": "#541805" },
      { "Sports Grey": "#C3C2BE" },
      { "Charcoal Dark Heather": "#2D2A2A" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-hoodedtop-s").attr("selected-value", clicked_color);
    }
  });

  // Premium
  $('[name="garmentcolor-hoodedtop-p"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Black: "#000000" },
      { Red: "#D8273A" },
      { Fushia: "#E52589" },
      { Lime: "#B5ED00" },
      { Apple: "#3CC66D" },
      { "Bottle Green": "#0F4823" },
      { "Sky Blue": "#BBE3FF" },
      { Purple: "#4A186B" },
      { Royal: "#002CB2" },
      { "French Navy": "#00065F" },
      { "Light Oxford": "#C3C2BE" }
    ],

    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#garmentcolor-hoodedtop-p").attr("selected-value", clicked_color);
    }
  });

  // INKS

  // These all use the same colours. Aaron can we move them into one thing?

  // Front 1

  $('[name="inkcolor-front-1"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-front-1").attr("selected-value", clicked_color);
    }
  });

  // Front 2

  $('[name="inkcolor-front-2"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-front-2").attr("selected-value", clicked_color);
    }
  });

  // Front 3

  $('[name="inkcolor-front-3"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-front-3").attr("selected-value", clicked_color);
    }
  });

  // Front 4

  $('[name="inkcolor-front-4"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-front-4").attr("selected-value", clicked_color);
    }
  });

  // Back 1

  $('[name="inkcolor-back-1"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-back-1").attr("selected-value", clicked_color);
    }
  });

  // Back 2

  $('[name="inkcolor-back-2"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-back-2").attr("selected-value", clicked_color);
    }
  });

  // Back 3

  $('[name="inkcolor-back-3"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-back-3").attr("selected-value", clicked_color);
    }
  });

  // Back 4

  $('[name="inkcolor-back-4"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-back-4").attr("selected-value", clicked_color);
    }
  });

  // Sleeve 1

  $('[name="inkcolor-sleeve-1"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-sleeve-1").attr("selected-value", clicked_color);
    }
  });

  // Sleeve 2

  $('[name="inkcolor-sleeve-2"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-sleeve-2").attr("selected-value", clicked_color);
    }
  });

  // Sleeve 3

  $('[name="inkcolor-sleeve-3"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-sleeve-3").attr("selected-value", clicked_color);
    }
  });

  // Sleeve 4

  $('[name="inkcolor-sleeve-4"]').paletteColorPicker({
    colors: [
      { White: "#FFFFFF" },
      { Cream: "#FBF9DE" },
      { "Light Grey": "#F1F1F2" },
      { Grey: "#C5C7C9" },
      { "Mid Grey": "#909295" },
      { "Dark Grey": "#626366" },
      { Charcoal: "#404041" },
      { Black: "#000000" },
      { Burgandy: "#6A0000" },
      { "Dark Red": "#B61318" },
      { Red: "#E21B23" },
      { "Warm Red": "#EE3E23" },
      { Peach: "#F27664" },
      { "Warm Yellow": "#FAA819" },
      { Orange: "#F6921E" },
      { "Bright Orange": "#F37B20" },
      { "Bright Yellow": "#FFF100" },
      { "Golden Yellow": "#FFDA00" },
      { "Pastel Yellow": "#FBF393" },
      { "Pastel Green": "#D7E7A8" },
      { "Lime Green": "#AACF37" },
      { "Bright Green": "#00A551" },
      { Green: "#00652E" },
      { "Bottle Green": "#00481A" },
      { Navy: "#0A0048" },
      { "Deep Blue": "#003878" },
      { Blue: "#0054A5" },
      { "Bright Blue": "#0091C1" },
      { Cyan: "#1EBFD8" },
      { "Light Blue": "#9BCCEE" },
      { Turquoise: "#00A8A0" },
      { Teal: "#007483" },
      { Purple: "#3B0B5E" },
      { Aubergine: "#59004E" },
      { Cerise: "#960059" },
      { "Light Pink": "#F7BAD5" },
      { Pink: "#F076AD" },
      { Fushia: "#EB008B" },
      { Coral: "#ED265C" },
      { "Flesh Tone": "#F3BB93" },
      { "Dark Brown": "#382602" },
      { Brown: "#604218" },
      { Chestnut: "#A4702A" },
      { "Flouresent Yellow": "#FFFF00" },
      { "Flouresent Pink": "#FF00FF" },
      { "Flouresent Green": "#7DFF42" },
      { "Flouresent Red": "#FF0000" },
      { "Flouresent Orange": "#FF9000" }
    ],
    clear_btn: "false",
    close_all_but_this: "true",
    onbeforeshow_callback: function(color_picker_button) {},
    onchange_callback: function(clicked_color) {
      $("#inkcolor-sleeve-4").attr("selected-value", clicked_color);
    }
  });
});

(function($) {
  $.fn.onEnter = function(func) {
    this.bind("keypress", function(e) {
      if (e.keyCode == 13) {
        e.preventDefault();
        func.apply(this, [e]);
      }
    });
    return this;
  };
})(jQuery);

function populateForm(form, data) {  $.each(data, function(
    key,
    value // all json fields ordered by name
  ) {
    var $ctrls = form.find("[name=" + key + "]"); //all form elements for a name. Multiple checkboxes can have the same name, but different values

    if ($ctrls.is("select")) {
      //special form types
      $("option", $ctrls).each(function() {
        if (this.value == value) this.selected = true;
      });
    } else if ($ctrls.is("textarea")) {
      $ctrls.val(value);
    } else {
      switch ($ctrls.attr("type")) { //input type
        case "text":
        case "hidden":
          $ctrls.val(value);
          break;
        case "radio":
          if ($ctrls.length >= 1) {
            $.each($ctrls, function(index) {
              // every individual element
              var elemValue = $(this).attr("value");
              var elemValueInData = (singleVal = value);
              if (elemValue === value) {
                $(this).prop("checked", true);
              } else {
                $(this).prop("checked", false);
              }
            });
          }
          break;
        case "checkbox":
          if ($ctrls.length > 1) {
            $.each($ctrls, function(
              index // every individual element
            ) {
              var elemValue = $(this).attr("value");
              var elemValueInData = undefined;
              var singleVal;
              for (var i = 0; i < value.length; i++) {
                singleVal = value[i];

                if (singleVal === elemValue) {
                  elemValueInData = singleVal;
                }
              }

              if (elemValueInData) {
                $(this).prop("checked", true);
                //$(this).prop('value', true);
              } else {
                $(this).prop("checked", false);
                //$(this).prop('value', false);
              }
            });
          } else if ($ctrls.length == 1) {
            $ctrl = $ctrls;
            if (value) {
              $ctrl.prop("checked", true);
            } else {
              $ctrl.prop("checked", false);
            }
          }
          break;
      } //switch input type
    }
  }); // all json fields
} // populate form

function addWorkDays(startDate, days) {
  // Get the day of the week as a number (0 = Sunday, 1 = Monday, .... 6 = Saturday)
  var dow = startDate.getDay();
  var daysToAdd = days;
  // If the current day is Sunday add one day
  if (dow == 0) daysToAdd++;
  // If the start date plus the additional days falls on or after the closest Saturday calculate weekends
  if (dow + daysToAdd >= 6) {
    //Subtract days in current working week from work days
    var remainingWorkDays = daysToAdd - (5 - dow);
    //Add current working week's weekend
    daysToAdd += 2;
    if (remainingWorkDays > 5) {
      //Add two days for each working week by calculating how many weeks are included
      daysToAdd += 2 * Math.floor(remainingWorkDays / 5);
      //Exclude final weekend if remainingWorkDays resolves to an exact number of weeks
      if (remainingWorkDays % 5 == 0) daysToAdd -= 2;
    }
  }
  startDate.setDate(startDate.getDate() + daysToAdd);
  return startDate;
}

/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

(function(global) {
  "use strict";

  var dateFormat = (function() {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZWN]|'[^']*'|'[^']*'/g;
    var timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g;
    var timezoneClip = /[^-+\dA-Z]/g;

    // Regexes and supporting functions are cached through closure
    return function(date, mask, utc, gmt) {
      // You can't provide utc if you skip other args (use the 'UTC:' mask prefix)
      if (
        arguments.length === 1 &&
        kindOf(date) === "string" &&
        !/\d/.test(date)
      ) {
        mask = date;
        date = undefined;
      }

      date = date || new Date();

      if (!(date instanceof Date)) {
        date = new Date(date);
      }

      if (isNaN(date)) {
        throw TypeError("Invalid date");
      }

      mask = String(
        dateFormat.masks[mask] || mask || dateFormat.masks["default"]
      );

      // Allow setting the utc/gmt argument via the mask
      var maskSlice = mask.slice(0, 4);
      if (maskSlice === "UTC:" || maskSlice === "GMT:") {
        mask = mask.slice(4);
        utc = true;
        if (maskSlice === "GMT:") {
          gmt = true;
        }
      }

      var _ = utc ? "getUTC" : "get";
      var d = date[_ + "Date"]();
      var D = date[_ + "Day"]();
      var m = date[_ + "Month"]();
      var y = date[_ + "FullYear"]();
      var H = date[_ + "Hours"]();
      var M = date[_ + "Minutes"]();
      var s = date[_ + "Seconds"]();
      var L = date[_ + "Milliseconds"]();
      var o = utc ? 0 : date.getTimezoneOffset();
      var W = getWeek(date);
      var N = getDayOfWeek(date);
      var flags = {
        d: d,
        dd: pad(d),
        ddd: dateFormat.i18n.dayNames[D],
        dddd: dateFormat.i18n.dayNames[D + 7],
        m: m + 1,
        mm: pad(m + 1),
        mmm: dateFormat.i18n.monthNames[m],
        mmmm: dateFormat.i18n.monthNames[m + 12],
        yy: String(y).slice(2),
        yyyy: y,
        h: H % 12 || 12,
        hh: pad(H % 12 || 12),
        H: H,
        HH: pad(H),
        M: M,
        MM: pad(M),
        s: s,
        ss: pad(s),
        l: pad(L, 3),
        L: pad(Math.round(L / 10)),
        t: H < 12 ? "a" : "p",
        tt: H < 12 ? "am" : "pm",
        T: H < 12 ? "A" : "P",
        TT: H < 12 ? "AM" : "PM",
        Z: gmt
          ? "GMT"
          : utc
            ? "UTC"
            : (String(date).match(timezone) || [""])
                .pop()
                .replace(timezoneClip, ""),
        o:
          (o > 0 ? "-" : "+") +
          pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
        S: ["th", "st", "nd", "rd"][
          d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10
        ],
        W: W,
        N: N
      };

      return mask.replace(token, function(match) {
        if (match in flags) {
          return flags[match];
        }
        return match.slice(1, match.length - 1);
      });
    };
  })();

  dateFormat.masks = {
    default: "ddd mmm dd yyyy HH:MM:ss",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:sso",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'",
    expiresHeaderFormat: "ddd, dd mmm yyyy HH:MM:ss Z"
  };

  // Internationalization strings
  dateFormat.i18n = {
    dayNames: [
      "Sun",
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "Sat",
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ],
    monthNames: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ]
  };

  function pad(val, len) {
    val = String(val);
    len = len || 2;
    while (val.length < len) {
      val = "0" + val;
    }
    return val;
  }

  /**
 * Get the ISO 8601 week number
 * Based on comments from
 * http://techblog.procurios.nl/k/n618/news/view/33796/14863/Calculate-ISO-8601-week-and-year-in-javascript.html
 *
 * @param  {Object} `date`
 * @return {Number}
 */
  function getWeek(date) {
    // Remove time components of date
    var targetThursday = new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate()
    );

    // Change date to Thursday same week
    targetThursday.setDate(
      targetThursday.getDate() - (targetThursday.getDay() + 6) % 7 + 3
    );

    // Take January 4th as it is always in week 1 (see ISO 8601)
    var firstThursday = new Date(targetThursday.getFullYear(), 0, 4);

    // Change date to Thursday same week
    firstThursday.setDate(
      firstThursday.getDate() - (firstThursday.getDay() + 6) % 7 + 3
    );

    // Check if daylight-saving-time-switch occurred and correct for it
    var ds =
      targetThursday.getTimezoneOffset() - firstThursday.getTimezoneOffset();
    targetThursday.setHours(targetThursday.getHours() - ds);

    // Number of weeks between target Thursday and first Thursday
    var weekDiff = (targetThursday - firstThursday) / (86400000 * 7);
    return 1 + Math.floor(weekDiff);
  }

  /**
 * Get ISO-8601 numeric representation of the day of the week
 * 1 (for Monday) through 7 (for Sunday)
 * 
 * @param  {Object} `date`
 * @return {Number}
 */
  function getDayOfWeek(date) {
    var dow = date.getDay();
    if (dow === 0) {
      dow = 7;
    }
    return dow;
  }

  /**
 * kind-of shortcut
 * @param  {*} val
 * @return {String}
 */
  function kindOf(val) {
    if (val === null) {
      return "null";
    }

    if (val === undefined) {
      return "undefined";
    }

    if (typeof val !== "object") {
      return typeof val;
    }

    if (Array.isArray(val)) {
      return "array";
    }

    return {}.toString.call(val).slice(8, -1).toLowerCase();
  }

  if (typeof define === "function" && define.amd) {
    define(function() {
      return dateFormat;
    });
  } else if (typeof exports === "object") {
    module.exports = dateFormat;
  } else {
    global.dateFormat = dateFormat;
  }
})(this);

/**
 * rsv.js - Really Simple Validation
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * v2.5.3, Nov 1 2014
 *
 * This powerful little script lets you add client-side validation to any webform with very little
 * work. It includes a number of pre-existing routines for common tasks like validating email
 * addresses, numbers, and other field content, and provides a simple mechanism to extend it to
 * whatever custom functions you need. For documentation and examples, please visit:
 *         http://www.benjaminkeen.com/software/rsv
 *
 * This script is written by Ben Keen with additional code contributed by Mihai Ionescu and Nathan
 * Howard. It is free to distribute, to re-write, spread on your toast - do what ever you want with it!
 */
{
  if (typeof rsv == "undefined") rsv = {};
}

// SETTINGS
//rsv.displayType          = "alert-all"; // "alert-one", "alert-all" or "display-html"
rsv.displayType = "display-html"; //line 549 - change this to use a <li> tags</li>
rsv.errorFieldClass = null;
rsv.errorTextIntro = "Oops, we need some more info before you can continue!";
rsv.errorJSItemBullet = "* ";
rsv.errorHTMLItemBullet = "&bull; ";
rsv.errorTargetElementId = "rsvErrors";
rsv.customErrorHandler = null;
rsv.onCompleteHandler = null;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/**
 * @param form the name attribute of the form to validate.
 * @param rules an array of the validation rules, each rule a string.
 * @return mixed returns a boolean (success/failure) for "alert-single" and "alert-all" options, and an
 *     array of arrays for return
 */
rsv.validate = function(form, rules) {
  rsv.returnHash = [];

  // loop through rules
  for (var i = 0; i < rules.length; i++) {
    // split row into component parts (replace any commas with %%C%% - they will be converted back later)
    var row = rules[i].replace(/\\,/gi, "%%C%%");
    row = row.split(",");

    // while the row begins with "if:..." test the condition. If true, strip the if:..., part and
    // continue evaluating the rest of the line. Keep repeating this while the line begins with an
    // if-condition. If it fails any of the conditions, don't bother validating the rest of the line
    var satisfiesIfConditions = true;
    while (row[0].match("^if:")) {
      var cond = row[0];
      cond = cond.replace("if:", "");

      // check if it's a = or != test
      var comparison = "equal";
      var parts = [];
      if (cond.search("!=") != -1) {
        parts = cond.split("!=");
        comparison = "not_equal";
      } else parts = cond.split("=");

      var fieldToCheck = parts[0];
      var valueToCheck = parts[1];

      // find value of FIELDNAME for conditional check
      var fieldnameValue = "";
      if (form[fieldToCheck].type == undefined) {
        // radio
        for (var j = 0; j < form[fieldToCheck].length; j++) {
          if (form[fieldToCheck][j].checked)
            fieldnameValue = form[fieldToCheck][j].value;
        }
      } else if (form[fieldToCheck].type == "checkbox") {
        // single checkbox
        if (form[fieldToCheck].checked) fieldnameValue = form[parts[0]].value;
      } else
        // all other field types
        fieldnameValue = form[parts[0]].value;

      // if the value is NOT the same, we don't need to validate this field. Return.
      if (comparison == "equal" && fieldnameValue != valueToCheck) {
        satisfiesIfConditions = false;
        break;
      } else if (comparison == "not_equal" && fieldnameValue == valueToCheck) {
        satisfiesIfConditions = false;
        break;
      } else row.shift(); // remove this if-condition from line, and continue validating line
    }

    if (!satisfiesIfConditions) continue;

    var requirement = row[0];
    var fieldName = row[1];
    var fieldName2, fieldName3, errorMessage, lengthRequirements, date_flag;

    // help the web developer out a little: this is a very common problem
    if (requirement != "function" && form[fieldName] == undefined) {
      alert(
        'RSV Error: the field "' +
          fieldName +
          "\" doesn't exist! Please check your form and settings."
      );
      return false;
    }

    // if the error field classes has been defined, ALWAYS assume that this field passes the
    // validation and set the class name appropriately (removing the errorFieldClass, if it exists). This
    // ensures that every time the form is submitted, only the fields that contain the latest errors have
    // the error class applied
    if (requirement != "function" && rsv.errorFieldClass) {
      if (form[fieldName].type == undefined) {
        // style each field individually
        for (var j = 0; j < form[fieldName].length; j++)
          rsv.removeClassName(form[fieldName][j], rsv.errorFieldClass, true);
      } else rsv.removeClassName(form[fieldName], rsv.errorFieldClass);
    }

    // depending on the validation test, store the incoming strings for use later...
    if (row.length == 6) {
      // valid_date
      fieldName2 = row[2];
      fieldName3 = row[3];
      date_flag = row[4];
      errorMessage = row[5];
    } else if (row.length == 5) {
      // reg_exp (WITH flags like g, i, m)
      fieldName2 = row[2];
      fieldName3 = row[3];
      errorMessage = row[4];
    } else if (row.length == 4) {
      // same_as, custom_alpha, reg_exp (without flags like g, i, m)
      fieldName2 = row[2];
      errorMessage = row[3];
    } else errorMessage = row[2]; // everything else!

    // if the requirement is "length...", rename requirement to "length" for switch statement
    if (requirement.match("^length")) {
      lengthRequirements = requirement;
      requirement = "length";
    }

    // if the requirement is "range=...", rename requirement to "range" for switch statement
    if (requirement.match("^range")) {
      rangeRequirements = requirement;
      requirement = "range";
    }

    // now, validate whatever is required of the field
    switch (requirement) {
      case "required":
        // if radio buttons or multiple checkboxes:
        if (form[fieldName].type == undefined) {
          var oneIsChecked = false;
          for (var j = 0; j < form[fieldName].length; j++) {
            if (form[fieldName][j].checked) oneIsChecked = true;
          }
          if (!oneIsChecked) {
            if (!rsv.processError(form[fieldName], errorMessage)) return false;
          }
        } else if (form[fieldName].type == "select-multiple") {
          var oneIsSelected = false;
          for (var k = 0; k < form[fieldName].length; k++) {
            if (form[fieldName][k].selected) oneIsSelected = true;
          }

          // if no options have been selected, or if there ARE no options in the multi-select
          // dropdown, return false
          if (!oneIsSelected || form[fieldName].length == 0) {
            if (!rsv.processError(form[fieldName], errorMessage)) return false;
          }
        } else if (form[fieldName].type == "checkbox") {
          // a single checkbox
          if (!form[fieldName].checked) {
            if (!rsv.processError(form[fieldName], errorMessage)) return false;
          }
        } else if (!form[fieldName].value) {
          // otherwise, just perform ordinary "required" check.
          if (!rsv.processError(form[fieldName], errorMessage)) return false;
        }
        break;

      case "digits_only":
        if (form[fieldName].value && form[fieldName].value.match(/\D/)) {
          if (!rsv.processError(form[fieldName], errorMessage)) return false;
        }
        break;

      case "letters_only":
        if (form[fieldName].value && form[fieldName].value.match(/[^a-zA-Z]/)) {
          if (!rsv.processError(form[fieldName], errorMessage)) return false;
        }
        break;

      case "is_alpha":
        if (form[fieldName].value && form[fieldName].value.match(/\W/)) {
          if (!rsv.processError(form[fieldName], errorMessage)) return false;
        }
        break;

      case "custom_alpha":
        var conversion = {
          L: "[A-Z]",
          V: "[AEIOU]",
          l: "[a-z]",
          v: "[aeiou]",
          D: "[a-zA-Z]",
          F: "[aeiouAEIOU]",
          C: "[BCDFGHJKLMNPQRSTVWXYZ]",
          x: "[0-9]",
          c: "[bcdfghjklmnpqrstvwxyz]",
          X: "[1-9]",
          E: "[bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ]"
        };

        var reg_exp_str = "";
        for (var j = 0; j < fieldName2.length; j++) {
          if (conversion[fieldName2.charAt(j)])
            reg_exp_str += conversion[fieldName2.charAt(j)];
          else reg_exp_str += fieldName2.charAt(j);
        }
        var reg_exp = new RegExp(reg_exp_str);

        if (
          form[fieldName].value &&
          reg_exp.exec(form[fieldName].value) == null
        ) {
          if (!rsv.processError(form[fieldName], errorMessage)) return false;
        }
        break;

      case "reg_exp":
        var reg_exp_str = fieldName2.replace(/%%C%%/gi, ",");

        if (row.length == 5) var reg_exp = new RegExp(reg_exp_str, fieldName3);
        else var reg_exp = new RegExp(reg_exp_str);

        if (
          form[fieldName].value &&
          reg_exp.exec(form[fieldName].value) == null
        ) {
          if (!rsv.processError(form[fieldName], errorMessage)) return false;
        }
        break;

      case "length":
        comparison_rule = "";
        rule_string = "";

        // if-else order is important here: needs to check for >= before >
        if (lengthRequirements.match(/length=/)) {
          comparison_rule = "equal";
          rule_string = lengthRequirements.replace("length=", "");
        } else if (lengthRequirements.match(/length>=/)) {
          comparison_rule = "greater_than_or_equal";
          rule_string = lengthRequirements.replace("length>=", "");
        } else if (lengthRequirements.match(/length>/)) {
          comparison_rule = "greater_than";
          rule_string = lengthRequirements.replace("length>", "");
        } else if (lengthRequirements.match(/length<=/)) {
          comparison_rule = "less_than_or_equal";
          rule_string = lengthRequirements.replace("length<=", "");
        } else if (lengthRequirements.match(/length</)) {
          comparison_rule = "less_than";
          rule_string = lengthRequirements.replace("length<", "");
        }

        // now perform the appropriate validation
        switch (comparison_rule) {
          case "greater_than_or_equal":
            if (!(form[fieldName].value.length >= parseInt(rule_string))) {
              if (!rsv.processError(form[fieldName], errorMessage))
                return false;
            }
            break;

          case "greater_than":
            if (!(form[fieldName].value.length > parseInt(rule_string))) {
              if (!rsv.processError(form[fieldName], errorMessage))
                return false;
            }
            break;

          case "less_than_or_equal":
            if (!(form[fieldName].value.length <= parseInt(rule_string))) {
              if (!rsv.processError(form[fieldName], errorMessage))
                return false;
            }
            break;

          case "less_than":
            if (!(form[fieldName].value.length < parseInt(rule_string))) {
              if (!rsv.processError(form[fieldName], errorMessage))
                return false;
            }
            break;

          case "equal":
            var range_or_exact_number = rule_string.match(/[^_]+/);
            var fieldCount = range_or_exact_number[0].split("-");

            // if the user supplied two length fields, make sure the field is within that range
            if (fieldCount.length == 2) {
              if (
                form[fieldName].value.length < fieldCount[0] ||
                form[fieldName].value.length > fieldCount[1]
              ) {
                if (!rsv.processError(form[fieldName], errorMessage))
                  return false;
              }
            } else {
              // otherwise, check it's EXACTLY the size the user specified
              if (form[fieldName].value.length != fieldCount[0]) {
                if (!rsv.processError(form[fieldName], errorMessage))
                  return false;
              }
            }

            break;
        }
        break;

      // this is also true if field is empty [should be same for digits_only]
      case "valid_email":
        if (form[fieldName].value && !rsv.isValidEmail(form[fieldName].value)) {
          if (!rsv.processError(form[fieldName], errorMessage)) return false;
        }
        break;

      case "valid_date":
        var isLaterDate = false;
        if (date_flag == "later_date") isLaterDate = true;
        else if (date_flag == "any_date") isLaterDate = false;

        if (
          !rsv.isValidDate(
            form[fieldName].value,
            form[fieldName2].value,
            form[fieldName3].value,
            isLaterDate
          )
        ) {
          if (!rsv.processError(form[fieldName], errorMessage)) return false;
        }
        break;

      case "same_as":
        if (form[fieldName].value != form[fieldName2].value) {
          if (!rsv.processError(form[fieldName], errorMessage)) return false;
        }
        break;

      case "range":
        comparison_rule = "";
        rule_string = "";

        // if-else order is important here: needs to check for >= before >
        if (rangeRequirements.match(/range=/)) {
          comparison_rule = "equal";
          rule_string = rangeRequirements.replace("range=", "");
        } else if (rangeRequirements.match(/range>=/)) {
          comparison_rule = "greater_than_or_equal";
          rule_string = rangeRequirements.replace("range>=", "");
        } else if (rangeRequirements.match(/range>/)) {
          comparison_rule = "greater_than";
          rule_string = rangeRequirements.replace("range>", "");
        } else if (rangeRequirements.match(/range<=/)) {
          comparison_rule = "less_than_or_equal";
          rule_string = rangeRequirements.replace("range<=", "");
        } else if (rangeRequirements.match(/range</)) {
          comparison_rule = "less_than";
          rule_string = rangeRequirements.replace("range<", "");
        }

        // now perform the appropriate validation
        switch (comparison_rule) {
          case "greater_than_or_equal":
            if (!(form[fieldName].value >= Number(rule_string))) {
              if (!rsv.processError(form[fieldName], errorMessage))
                return false;
            }
            break;

          case "greater_than":
            if (!(form[fieldName].value > Number(rule_string))) {
              if (!rsv.processError(form[fieldName], errorMessage))
                return false;
            }
            break;

          case "less_than_or_equal":
            if (!(form[fieldName].value <= Number(rule_string))) {
              if (!rsv.processError(form[fieldName], errorMessage))
                return false;
            }
            break;

          case "less_than":
            if (!(form[fieldName].value < Number(rule_string))) {
              if (!rsv.processError(form[fieldName], errorMessage))
                return false;
            }
            break;

          case "equal":
            var rangeValues = rule_string.split("-");

            // if the user supplied two length fields, make sure the field is within that range
            if (
              form[fieldName].value < Number(rangeValues[0]) ||
              form[fieldName].value > Number(rangeValues[1])
            ) {
              if (!rsv.processError(form[fieldName], errorMessage))
                return false;
            }
            break;
        }
        break;

      case "function":
        custom_function = fieldName;
        eval("var result = " + custom_function + "()");

        if (result.constructor.toString().indexOf("Array") != -1) {
          for (var j = 0; j < result.length; j++) {
            if (!rsv.processError(result[j][0], result[j][1])) return false;
          }
        }
        break;

      default:
        alert("Unknown requirement flag in validateFields(): " + requirement);
        return false;
    }
  }

  // if the user has defined a custom event handler, pass the information to it
  if (typeof rsv.customErrorHandler == "function") {
    if (!rsv.customErrorHandler(form, rsv.returnHash)) {
      return false;
    }
  } else if (rsv.displayType == "alert-all") {
    // if the user has chosen "alert-all" or "return-errors", perform the appropriate action
    var errorStr = rsv.errorTextIntro + "\n\n";
    for (var i = 0; i < rsv.returnHash.length; i++) {
      errorStr += rsv.errorJSItemBullet + rsv.returnHash[i][1] + "\n";

      // apply the error CSS class (if defined) all the fields and place the focus on the first
      // offending field

      rsv.styleField(rsv.returnHash[i][0], i == 0);
    }

    if (rsv.returnHash.length > 0) {
      alert(errorStr);
      return false;
    }
  } else if (rsv.displayType == "display-html") {
    var success = rsv.displayHTMLErrors(form, rsv.returnHash);

    // if it wasn't successful, just return false to stop the form submit, otherwise continue processing
    if (!success) return false;
  }

  // finally, if the user has specified a custom onCompleteHandler, use it
  if (typeof rsv.onCompleteHandler == "function")
    return rsv.onCompleteHandler();
  else return true;
};

/**
 * Processes an error message, the behaviour of which is according to the rsv.displayType setting.
 * It either alerts the error (with "alert-one") or stores the field node and error message in a
 * hash to return / display once all rules are processed.
 *
 * @param obj the offending form field
 * @param message the error message string
 * @return boolean whether or not to continue processing
 */
rsv.processError = function(obj, message) {
  message = message.replace(/%%C%%/gi, ",");

  var continueProcessing = true;
  switch (rsv.displayType) {
    case "alert-one":
      alert(message);
      rsv.styleField(obj, true);
      continueProcessing = false;
      break;

    case "alert-all":
    case "display-html":
      rsv.returnHash.push([obj, message]);
      break;
  }

  return continueProcessing;
};

/**
 * This function is the default handler for the "display-html" display type. This generates the errors
 * as HTML and inserts them into the target node (rsv.errorTargetElementId).
 */
rsv.displayHTMLErrors = function(f, errorInfo) {
  //ensure that we clear the HTML, to remove any old error messages from within it.
  document.getElementById(rsv.errorTargetElementId).innerHTML = "";

  var errorHTML = rsv.errorTextIntro + "<ul>";
  for (var i = 0; i < errorInfo.length; i++) {
    // errorHTML += rsv.errorHTMLItemBullet + errorInfo[i][1] + "</li>";
    errorHTML += "<li>" + errorInfo[i][1] + "</li>";
    rsv.styleField(errorInfo[i][0], i == 0);
  }
  errorHTML += "</ul>";

  if (errorInfo.length > 0) {
    // document.getElementById(rsv.errorTargetElementId).style.display = "block";
    document.getElementById(rsv.errorTargetElementId).innerHTML = errorHTML;
    return false;
  }

  return true;
};

/**
 * Highlights a field that fails a validation test IF the errorFieldClass setting is set. In addition,
 * if the focus parameter is set to true, it places the focus on the field.
 *
 * @param the offending form field element
 * @param boolean whether or not to place the mouse focus on the field
 */
rsv.styleField = function(field, focus) {
  if (!rsv.errorFieldClass) return;

  // if "field" is an array, it's a radio button. Focus on the first element
  if (field.type == undefined) {
    if (focus) field[0].focus();

    // style each field individually
    for (var i = 0; i < field.length; i++)
      rsv.addClassName(field[i], rsv.errorFieldClass, true);
  } else {
    if (rsv.errorFieldClass) rsv.addClassName(field, rsv.errorFieldClass, true);
    if (focus) field.focus();
  }
};

/**
 * Tests a string is a valid email. NOT the most elegant function...
 */
rsv.isValidEmail = function(str) {
  var str2 = str.replace(/^\s*/, "");
  var s = str2.replace(/\s*$/, "");

  var at = "@";
  var dot = ".";
  var lat = s.indexOf(at);
  var lstr = s.length;

  if (
    s.indexOf(at) == -1 ||
    (s.indexOf(at) == -1 || s.indexOf(at) == 0 || s.indexOf(at) == lstr) ||
    (s.indexOf(dot) == -1 || s.indexOf(dot) == 0 || s.indexOf(dot) == lstr) ||
    s.indexOf(at, lat + 1) != -1 ||
    (s.substring(lat - 1, lat) == dot ||
      s.substring(lat + 1, lat + 2) == dot) ||
    s.indexOf(dot, lat + 2) == -1 ||
    s.indexOf(" ") != -1
  ) {
    return false;
  }

  return true;
};

/**
 * Returns true if string parameter is empty or whitespace characters only.
 */
rsv.isWhitespace = function(s) {
  var whitespaceChars = " \t\n\r\f";
  if (s == null || s.length == 0) return true;

  for (var i = 0; i < s.length; i++) {
    var c = s.charAt(i);
    if (whitespaceChars.indexOf(c) == -1) return false;
  }

  return true;
};

/*
 * Checks incoming date is valid. If any of the date parameters fail, it returns a string
 * message denoting the problem.
 *
 * @param month an integer between 1 and 12
 * @param day an integer between 1 and 31 (depending on month)
 * @year a 4-digit integer value
 * @isLaterDate a boolean value. If true, the function verifies the date being passed in is LATER
 *   than the current date
 */
rsv.isValidDate = function(month, day, year, isLaterDate) {
  // depending on the year, calculate the number of days in the month
  var daysInMonth;
  if (
    year % 4 == 0 &&
    (year % 100 != 0 || year % 400 == 0) // LEAP YEAR
  )
    daysInMonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  else daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  // check the incoming month and year are valid
  if (!month || !day || !year) return false;
  if (1 > month || month > 12) return false;
  if (year < 0) return false;
  if (1 > day || day > daysInMonth[month - 1]) return false;

  // if required, verify the incoming date is LATER than the current date.
  if (isLaterDate) {
    // get current date
    var today = new Date();
    var currMonth = today.getMonth() + 1; // since returns 0-11
    var currDay = today.getDate();
    var currYear = today.getFullYear();

    // zero-pad today's month & day
    if (String(currMonth).length == 1) currMonth = "0" + currMonth;
    if (String(currDay).length == 1) currDay = "0" + currDay;
    var currDate = String(currYear) + String(currMonth) + String(currDay);

    // zero-pad incoming month & day
    if (String(month).length == 1) month = "0" + month;
    if (String(day).length == 1) day = "0" + day;
    incomingDate = String(year) + String(month) + String(day);

    if (Number(currDate) > Number(incomingDate)) return false;
  }

  return true;
};

rsv.addClassName = function(objElement, strClass, mayAlreadyExist) {
  if (objElement.className) {
    var arrList = objElement.className.split(" ");

    if (mayAlreadyExist) {
      var strClassUpper = strClass.toUpperCase();

      // find all instances and remove them
      for (var i = 0; i < arrList.length; i++) {
        if (arrList[i].toUpperCase() == strClassUpper) {
          arrList.splice(i, 1);
          i--;
        }
      }
    }

    arrList[arrList.length] = strClass;
    objElement.className = arrList.join(" ");
  } else objElement.className = strClass;
};

rsv.removeClassName = function(objElement, strClass) {
  if (objElement.className) {
    var arrList = objElement.className.split(" ");
    var strClassUpper = strClass.toUpperCase();

    for (var i = 0; i < arrList.length; i++) {
      if (arrList[i].toUpperCase() == strClassUpper) {
        arrList.splice(i, 1);
        i--;
      }
    }

    objElement.className = arrList.join(" ");
  }
};
