<?php

include_once('common/common.php');
session_start();


$loggedIn = isset($_SESSION["LOGGED_IN"]);
parse_str($_SERVER['QUERY_STRING']);

if($loggedIn == false)
{
  //You need to redirect
    header("Location: AdminLogin.php?orderNumber=".$orderNumber); /* Redirect browser */
    exit();
}
 
 
    parse_str($_SERVER['QUERY_STRING']);

    $fileName = '';
    $directoryToSearch = 'uploads/'.$orderNumber .'/*';
    $directory = 'uploads/'.$orderNumber.'/';

    $frontFiles= array();
    $backFiles = array();

    foreach(glob($directoryToSearch) as $file) 
    {  
        $pathinfo = pathinfo($file);
        $fileName = $pathinfo['basename']; // as well as other data in array print_r($pathinfo);
        if (strpos($fileName, 'Front') !== false) 
        {
          $frontFiles[] = $fileName;
        }
        else
        {
          $backFiles[] = $fileName;
        }
    }


//need to define all of the variables, get them from the database, then echo them out on the page.. :(

    $pdo = new PDO($DB_CONNECTION_STRING, $DB_USER_NAME, $DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $sql= "SELECT * FROM OrderHeader WHERE ID = :orderID";
    $stmt = $pdo->prepare($sql); 
    $stmt->bindParam(':orderID', $orderNumber);
    $stmt->execute();
    $row = $stmt->fetchObject();

    $firstName = $row->Name;
    $orderDate = $row->OrderDate;
    $phoneNumber = $row->PhoneNumber;
    $emailAddress = $row->EmailAddress;
    $addressLine1 = $row->DeliveryAddressLine1;
    $addressLine2 = $row->DeliveryAddressLine2;
    $addressLine3 = $row->DeliveryAddressLine3;
    $billingAddressLine1 = $row->OrderAddressLine1;
    $billingAddressLine2 = $row->OrderAddressLine2;
    $billingAddressLine3 = $row->OrderAddressLine3;
    $billingCity = $row->OrderCity;
    $billingPostCode = $row->OrderPostCode;
    $city = $row->DeliveryCity;
    $postCode = $row->DeliveryPostCode;
    $orderTotal = $row->OrderTotal;

    $sql="select count(*) from OrderDetail where orderHeaderID = :orderHeaderID";
    $stmt2 = $pdo->prepare($sql); 
    $stmt2->bindParam(':orderHeaderID', $orderNumber);
    $stmt2 -> execute();
    $garmentCount = $stmt2->fetchColumn();

    $query = "SELECT * FROM OrderDetail where OrderHeaderID = :orderHeaderID";
    $stmt2 = $pdo->prepare($query);
    $stmt2->bindParam(':orderHeaderID', $orderNumber);
    $stmt2->execute();
    $data = $stmt2->fetchAll();

?>



<!DOCTYPE html>
<html>

<head>
  <title>Order <?php echo $orderNumber ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
  <link href="css/app.css" rel="stylesheet">
  <script type="text/javascript" src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
<link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="icons/favicon-16x16.png">
<link rel="manifest" href="icons/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="icons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
</head>
<div class="wrapper">
  <header class="header">
    <div class="container">
      <h1 class="logo">Tshirt-Takeaway</h1>
    </div>
  </header>
  <div class="content">
    <div class="container">
      <h2 class="title">Order Ref <?php echo $orderNumber ?></h2>
      <div class="order-row">
        <p class="longform">This order contains <strong><?php echo $garmentCount; ?></strong> garments.</p>
      </div>
    </div>
    <div class="container">
      <div class="order-row">
          <p class="label--standalone">Customer Details:</p>
          <ul class="order-details">
            <li>Ordered: <span><?php echo $orderDate; ?></span></li>
            <li>By: <span><?php echo $firstName; ?></span></li>
            <li>Contact number: <span><?php echo $phoneNumber; ?></span></li>
            <li>Email address: <a href="mailto:<?php echo $emailAddress; ?>"><?php echo $emailAddress; ?></a></li>
          </ul>
      </div>
  </div>
<div class="container">
  <div class="order-row">
    <div class="one-half">
      <p class="label--standalone">Delivery Address:</p>
      <ul class="order-details">
        <li><span><?php echo $addressLine1; ?></span></li>
        <li><span><?php echo $addressLine2; ?></span></li>
        <li><span><?php echo $addressLine3; ?></span></li>
        <li><span><?php echo $city; ?></span></li>
        <li><span><?php echo $postCode; ?></span></li>
      </ul>
    </div>
    <div class="one-half">
          <p class="label--standalone">Billing Address:</p>
          <ul class="order-details">
            <li><span><?php echo $billingAddressLine1; ?></span></li>
            <li><span><?php echo $billingAddressLine2; ?></span></li>
            <li><span><?php echo $billingAddressLine3; ?></span></li>
            <li><span><?php echo $billingCity; ?></span></li>
            <li><span><?php echo $billingPostCode; ?></span></li>
          </ul>
</div>
</div>
</div>
  <div class="container">
      <section class="order__total"  style="margin-bottom: 60px;">
        <span class="total-label">Total Paid:</span>
        <span class="total-price" id="totalSummary">£<?php echo $orderTotal ?></span>
      </section>
    </div>


    <?php
        $currentGarmentNumber = 1;
          foreach($data as $row) {
              $colour = $row['Colour'];
              $garmentType = $row['GarmentType'];


    ?><div class="page-break"></div>

                <div class="container">
                <h3 class="title--smaller">Garment <?php echo $currentGarmentNumber; ?></h3>
                <div class="order-row">
                    <div class="one-half">
                    <p class="label--standalone">Garments:</p>
                    <ul class="order-details">
                        <li>Garment: <span><?php echo $row['GarmentType'] ?></span></li>
                        <li>Quality: <span><?php echo $row['Quality'] ?></span></li>
                        <li>Colour: <span><?php echo $row['Colour'] ?></span></li>
                        <li>Quantity: <span><?php echo $row['Quantity'] ?></span></li>
                    </ul>
                    </div>
                    <div class="one-half">
                    <p class="label--standalone">Sizes:</p>
                    <ul class="order-details">
                        <li>Small: <span><?php echo $row['SizeSmall'] ?></span></li>
                        <li>Medium: <span><?php echo $row['SizeMedium'] ?></span></li>
                        <li>Large: <span><?php echo $row['SizeLarge'] ?></span></li>
                        <li>XLarge: <span><?php echo $row['SizeXLarge'] ?></span></li>
                        <li>2XLarge: <span><?php echo $row['Size2XLarge'] ?></span></li>
                    </ul>
                    </div>
                </div>
                <!-- End order row -->
                <div class="order-row">
                    <div class="one-half">
                    <p class="label--standalone">Front Uploads:</p>
                    <ul class="order-details">
                      <?php
                        foreach($frontFiles as $frontFile)
                        {

                          $garmentNumberToCheck = (string)$currentGarmentNumber;
                            
                          if(strpos($frontFile, $garmentNumberToCheck )== true)
                          {
                            $formattedString = "<li><a href=".$directory.$frontFile .">".$frontFile."</a></li>";
                            echo $formattedString;
                          }
                        }

                        ?>
                    </ul>
                    </div>
                    <div class="one-half">
                    <p class="label--standalone">Back Uploads:</p>
                    <ul class="order-details">
                        <?php
                        foreach($backFiles as $backFile)
                        {
                          $garmentNumberToCheck = (string)$currentGarmentNumber;

                          if(strpos($backFile, $garmentNumberToCheck )== true)
                          {
                            $formattedString = "<li><a href=".$directory.$backFile .">".$backFile."</a></li>";
                            echo $formattedString;
                          }
                        }

                        ?>
                    </ul>
                    </div>
                </div>

                <div class="order-row">
                    <div class="one-half">
                    <p class="label--standalone">Number Of Colours:</p>
                    <ul class="order-details">
                        <li>Front: <span><?php echo $row['ColourFrontTotal'] ?></span></li>
                        <li>Back: <span><?php echo $row['ColourBackTotal'] ?></span></li>
                        <li>Sleeve: <span><?php echo $row['ColourSleeveTotal'] ?></span></li>
                    </ul>
                    </div>
                    <div class="one-half">
                    <p class="label--standalone">Selected Colours:</p>

                    <ul class="order-details">
                        <li>Front: <span><?php echo $row['ColourFront1'] ?></span> <span><?php echo $row['ColourFront2'] ?></span> <span><?php echo $row['ColourFront3'] ?></span> <span><?php echo $row['ColourFront4'] ?></span>. <?php  ?></li>
                        <li>Back: <span><?php echo $row['ColourBack1'] ?></span> <span><?php echo $row['ColourBack2'] ?></span> <span><?php echo $row['ColourBack3'] ?></span> <span><?php echo $row['ColourBack4'] ?></span>.</li>
                        <li>Sleeve: <span><?php echo $row['ColourSleeve1'] ?></span> <span><?php echo $row['ColourSleeve2'] ?></span> <span><?php echo $row['ColourSleeve3'] ?></span> <span><?php echo $row['ColourSleeve4'] ?></span></li>
                    </ul>
                    </div>
                </div>
                <div class="order-row">
                    <p class="order-details">Any other info: <span><?php echo $row['AnyOtherInformation'] ?></span></p>
                </div>
                </div>
        <?php 
          $currentGarmentNumber++;
            }
        ?>
  </div>
</div>
</div>
<footer class="footer">
  <div class="container">
    <img src="img/logo-long.svg" alt="Tshirt Takeaway" class="footer__logo">
    <section class="social">
      <ul>
        <li>
          <a class="facebook social-icon" href="https://www.facebook.com/tshirttakeawayuk" target="_blank">
              <span>Facebook</span>
            </a>
        </li>
        <li>
          <a href="https://twitter.com/tshirt_takeaway" class="twitter social-icon" href="#" target="_blank">
              <span>Twitter</span>
            </a>
        </li>
        <li>
          <a href="#" class="instagram social-icon" href="https://www.instagram.com/tshirt_takeaway/" target="_blank">
              <span>Instagram</span>
            </a>
        </li>
      </ul>
    </section>
  </div>
</footer>
</body>

</html>