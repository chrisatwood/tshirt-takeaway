<?php

require_once('common/common.php');
session_start();
$session_id = session_id();
	
$output_dir = "uploads/";
$allowed =  array('gif','png' ,'jpg', 'jpeg', 'ai', 'psd', 'pdf');

if(isset($_FILES["fileUpload"])&& isset($_POST["stage"]) && isset($_POST["detail"]))
{
	$ret = array();
	
	$error =$_FILES["fileUpload"]["error"];
	if(!is_array($_FILES["fileUpload"]["name"])) //single file
	{
 	 	$fileName = $_FILES["fileUpload"]["name"];
	
		$stageName = $_POST["stage"];
		$detail = $_POST["detail"];

		$path_parts = pathinfo($_FILES["fileUpload"]["name"]);
		$extension = $path_parts['extension'];

		//if extension is safe do below, othwerise delete it
		if(!in_array($extension, $allowed) ) {
			$custom_error= array();
			$custom_error['jquery-upload-file-error'] = "File format not allowed";
			echo json_encode($custom_error);
			die();
		}

		$fileNameFormatted = $stageName."_".$detail.".".$extension; //i.e. FrontB_1.png
		$destinationPath = $output_dir.$session_id. "/". $fileNameFormatted;

		$exists = file_exists($output_dir.$session_id);
		if($exists == false)
		{
			mkdir($output_dir.$session_id);
		} 

 		move_uploaded_file($_FILES["fileUpload"]["tmp_name"], $destinationPath);

    	$ret[]= $destinationPath;
	}
	else  //Multiple files, file[]
	{
	  $fileCount = count($_FILES["myfile"]["name"]);
	  for($i=0; $i < $fileCount; $i++)
	  {
	  	$fileName = $_FILES["myfile"]["name"][$i];
		move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$fileName); //make a new file name here
	  	$ret[]= $fileName;
	  }
	
	}
    echo json_encode($ret);
 }
 ?>