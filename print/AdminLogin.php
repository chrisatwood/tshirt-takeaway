<?php

include_once('common/common.php');
session_start();
parse_str($_SERVER['QUERY_STRING']);


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //validate the user credentials

    $userName = $_POST['userName'];
    $password = $_POST['password'];

    if($userName === $ADMIN_USER_NAME && $password === $ADMIN_USER_PASSWORD)
    {
        //need to grab the ID from the querystring (if there) and redirect to that page, 
        //otherwise just go to the summary
        $_SESSION['LOGGED_IN'] = true;
        if(isset($orderNumber))
        {
          header("Location: AdminOrderSummary.php?orderNumber=".$orderNumber); /* Redirect browser */
          exit();
        }
        else
        {
          header("Location: AdminOrderList.php"); /* Redirect browser */
          exit();
        }
    }
    else
    {
        header("Location: AdminLogin.php"); /* Redirect browser */
        exit();
    }
}
?>

<!DOCTYPE html>
  <html>
    <head>
      <title>Tshirt Takeaway</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
       <link href="css/app.css" rel="stylesheet">
    <script type="text/javascript" src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="icons/favicon-16x16.png">
<link rel="manifest" href="icons/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="icons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
</head>
<div class="wrapper">
  <header class="header">
    <div class="container">      
      <h1 class="logo">Tshirt-Takeaway</h1>
    </div>
  </header>
  <div class="content">
    <div class="container container--center">
      <h2 class="title">Login</h2>
      <p class="longform last">Please Enter your Login Credentials</p>
            <form  method="post">
                <div class="row">
                    <div class="one-half">
                        <label class="label" for="userName">User Name </label>
                       <label class="label" for="password">Password </label>
                   </div>
                    <div class="one-half">
                        <input class="input" type="text" name="userName">
                        <input class="input" type="password" name="password" >
                    </div>
                </div>
                <button class="btn--smaller next" type="input">Login </button>
            </form>
    </div>
  </div>
</div>
<footer class="footer">
  <div class="container">
    <img src="img/logo-long.svg" alt="Tshirt Takeaway" class="footer__logo">
      <section class="social">
        <ul>
          <li>
            <a class="facebook social-icon" href="https://www.facebook.com/tshirttakeawayuk" target="_blank">
              <span>Facebook</span>
            </a>
          </li>
          <li>
          <a href="https://twitter.com/tshirt_takeaway" class="twitter social-icon" href="#" target="_blank">
              <span>Twitter</span>
            </a>
          </li>
          <li>
            <a href="#" class="instagram social-icon" href="https://www.instagram.com/tshirt_takeaway/" target="_blank">
              <span>Instagram</span>
            </a>
          </li>
        </ul>
      </section>
    </div>
  </footer>
</body>
</html>