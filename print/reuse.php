<?php

require_once('common/common.php');
session_start();
$session_id = session_id();
$ret = array();
	
$output_dir = "uploads/";

    $directoryToSearch = $output_dir.$session_id .'/*';
	
    //check if the post has some data in it

     if(isset($_POST["detail"]))
     {
        $detailNumber = $_POST["detail"];
        $directory = 'uploads/'.$session_id.'/';

        $frontFiles= array();
        $backFiles = array();

        foreach(glob($directoryToSearch) as $fileName) 
        {  
            $details = array();
            $pathinfo = pathinfo($fileName);

            $extension = $pathinfo['extension'];
            $originalFileName = $pathinfo['filename']; 
            $tempFileName = substr($originalFileName, 0, -1); //get it without the number on the end
            $newFileName = $tempFileName.$detailNumber;
            //copy the file over...
            copy($directory.$originalFileName.".".$extension, $directory.$newFileName.".".$extension);

            $details['path'] = $directory.$newFileName.".".$extension;
            $details['key'] = $newFileName; //etc.
            $ret[] = $details;
        }

        echo json_encode($ret);

    }
 ?>